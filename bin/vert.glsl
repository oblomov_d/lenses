#version 430 core

layout(location = 0) in vec2 vertex_position;
layout(location = 1) in vec3 vertex_color;

out vec3 VColor;

uniform float uni_width;
uniform float uni_height;
uniform float uni_scaling;
uniform float uni_yshift;

vec3 magic_color = vec3(0.0f, 0.0f, 0.4f);

void main(){
    VColor = vertex_color;

    vec2 vpos = vertex_position;
    vpos.y += uni_yshift;
    vec4 pos = vec4(vpos * uni_scaling, 1.0, 1.0);
    pos.y = pos.y * uni_width / uni_height;
    gl_Position = pos;
}
