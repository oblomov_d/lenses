#version 430 core

in vec3 VColor;

layout(location = 0) out vec4 fragColor;

void main(){
    fragColor = vec4(VColor, 1.0f);
}
