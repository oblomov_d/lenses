#pragma once

#include <src/input.h>
#include <src/lense.h>
#include <src/mesh.h>
#include <src/shapes.h>

#include <AntTweakBar.h>

#include <vector>


struct Scene {
    ~Scene();
    void init(int lcnt=5, LENSE_TYPE *ltypes=nullptr, float *lxs=nullptr,
	      float *lfls=nullptr, float ax=-25, float alen=5,
	      bool at_inf=false, float inf_sign=-1,
	      bool dticks=true, bool darrows=true, bool drays=true);
    void cleanup();
    void initBars();
    void update(const Input &inp);
    void updateBars();
    void updateSelectedLense();
    void draw();
    void addSeg(const seg2 &s, int color_ind);
    void saveToFile(char *filename);
    void initFromFile(char *filename);

    bool initialized = false;

    int lense_cnt = 0;
    Lense *lenses = nullptr;
    int selected_lense = -1;
    int last_selected_lense = -1;

    LENSE_TYPE *target_types;
    int last_edited_lense;
    
    TwBar *editor;
    TwBar *selector;
    TwBar *settings;
    
    bool display_rays = true;
    std::vector<int> seg_color_inds;
    std::vector<seg2> segs;
    Mesh seg_mesh;

    bool selected_arrow = false; 
    Arrow arrow;
    std::vector<Arrow> arrows;
    bool display_arrows = true;
    Mesh arrow_mesh;

    Mesh axis_mesh;
    bool display_ticks = true;
    Mesh ticks_mesh;
    // will be used by Renderer to set uniform in shader;
    const float scale = 0.03f;
    const float xshift = 0.0f;
    const float yshift = -6.0f;
    // should also somehow determine lengths between notches
    // 1 notch = ? cm
    const float notch_len = 1.00f;
};


