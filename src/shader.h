#pragma once

#include <GL/glew.h>
#include <GL/gl.h>

GLuint loadShaders(const char* vert_file_path, const char* frag_file_path);

