#pragma once

#include <GL/glew.h>
#include <GL/gl.h>

#include <iostream>
#include <vector>

#include <src/vecmath.h>

struct Mesh {
    std::vector<triangle> vertex_v;
    std::vector<float3> color_v;
    int elem_cnt = 0;
    GLuint vertexArrayID;
    GLuint vertexbuffer, colorbuffer;
    GLenum drawMode = GL_TRIANGLES;

    void reserve(int cnt);
    // void addPoint(const float2 &p, const float3 &color); 
    void addTriangle(const triangle &t, const float3 &color);
    void move(const float2 &ofs);

    ~Mesh();

    //assumes gl is initialized
    void init();
    void clear();
    void buffer();
    void draw();
};

std::ostream &operator << (std::ostream &os, const Mesh &m);

