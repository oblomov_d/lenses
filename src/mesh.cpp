#include <src/mesh.h>

#include <iostream>
#include <vector>

#include <src/vecmath.h>

#undef DEBUGGING

void Mesh::init(){
    elem_cnt = 0;
    glGenVertexArrays(1, &vertexArrayID);
    glGenBuffers(1, &vertexbuffer);
    glGenBuffers(1, &colorbuffer);
    
    // uncertain it should be there
    drawMode = GL_TRIANGLES;
    #ifdef DEBUGGING
    std::cout << "generated mesh with ids " << vertexArrayID <<
	", " << vertexbuffer << ", " << colorbuffer << "\n";
    #endif
}

void Mesh::reserve(int cnt) {
    vertex_v.reserve(elem_cnt / 3 + cnt);
    color_v.reserve(elem_cnt + cnt * 3);
}

/*
void Mesh::addPoint(const float2 &p, const float3 &color) {
    elem_cnt++;
    vertex_v.push_back(p);
    color_v.push_back(color);
}
*/

void Mesh::addTriangle(const triangle &t, const float3 &color) {
    elem_cnt += 3;
    vertex_v.push_back(t);
    /*
    color_v.push_back(float3(1.0, 0.0, 0.0));
    color_v.push_back(float3(0.0, 1.0, 0.0));
    color_v.push_back(float3(0.0, 0.0, 1.0));
    */
    color_v.push_back(color);
    color_v.push_back(color);
    color_v.push_back(color);
}

void Mesh::move(const float2 &ofs) {
    for (int i = 0; i < vertex_v.size(); i++) {
	vertex_v[i].a += ofs;
	vertex_v[i].b += ofs;
	vertex_v[i].c += ofs;
    }
}


Mesh::~Mesh(){
    glDeleteBuffers(1, &colorbuffer);
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteVertexArrays(1, &vertexArrayID);
}

void Mesh::clear(){
    vertex_v.clear();
    color_v.clear();
    elem_cnt = 0;
}

void Mesh::buffer(){
    glBindVertexArray(vertexArrayID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);

    int v_buf_size = 6 * vertex_v.size() * sizeof(float);
    glBufferData(GL_ARRAY_BUFFER, v_buf_size,
	   	 &vertex_v[0], GL_STATIC_DRAW);

    int col_buf_size = 3 * color_v.size() * sizeof(float);
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glBufferData(GL_ARRAY_BUFFER, col_buf_size, 
		 &color_v[0], GL_STATIC_DRAW);
}

void Mesh::draw(){
    glBindVertexArray(vertexArrayID);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

    #ifdef DEBUGGING
    std::cout << "drawing " << elem_cnt << " elements\n";
    #endif

    //drawMode = GL_LINE_STRIP;
    //drawMode = GL_TRIANGLES;
    glDrawArrays(drawMode, 0, elem_cnt); 

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

std::ostream &operator << (std::ostream &os, const Mesh &m) {
    std::cout << "Mesh: {\n";
    std::cout << "\telem_cnt = " << m.elem_cnt << "\n"; 
    std::cout << "\tvertex_v: (";
    float2 *vp = (float2*) &m.vertex_v[0];
    if (m.elem_cnt > 0) {
	for (int i = 0; i < m.elem_cnt-1; i++) {
	    std::cout << vp[i] << ", "; 
	}
	std::cout << vp[m.elem_cnt-1];
    }
    std::cout << ")\n";
    std::cout << "}\n";
    return os;
}

