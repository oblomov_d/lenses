#pragma once

#include <src/shapes.h>

#include <AntTweakBar.h>

typedef enum { EMPTY = 0, POS_MENISCUS = 1, BICONVEX = 2,
	       PLANOCONVEX = 3, PLANOCONCAVE = 4, NEG_MENISCUS = 5,
	       BICONCAVE = 6} LENSE_TYPE; 

bool ltypeFromString(const char* cstr, LENSE_TYPE &res);

struct LenseParams final {
    float refr_c;
    float ir1;
    float ir2;
    float thick;
    float focal_len = 1.0f;
    bool can_focus = true;

    void calcF(float n, float iR1, float iR2, float d);
    void recalcF();
    void recalcRefr();
};

void TW_CALL lenseSetRefrC(const void *val, void *client_data);
void TW_CALL lenseGetRefrC(void *val, void *client_data);
void TW_CALL lenseSetF(const void *val, void *client_data);
void TW_CALL lenseGetF(void *val, void *client_data);
void TW_CALL lenseSetX(const void *val, void *client_data);
void TW_CALL lenseGetX(void *val, void *client_data);
    
struct Lense final {
    shape *sh = nullptr;
    float2 pos;
    float2 dims;
    LenseParams params;
    Mesh mesh;
    LENSE_TYPE type = LENSE_TYPE::EMPTY;

    void initEmpty(float2 p);
    void initSphere(float2 p, float refr_c, float r);
    void initBiconvex(float2 p, float refr_c, float w, float h, float r);
    void initPlanoConvex(float2 p, float refr_c, float w, float h, float r);
    void initPlanoConcave(float2 p, float refr_c, float d, float w, float h,
			  float r);
    void initBiconcave(float2 p, float refr_c, float d, float w, float h,
		       float r);
    void initMeniscus(float2 p, float refr_c, float d, float w, float h,
		      float r1, float r2);
    
    float2 getFocalPoint() const;
    bool testIntersect(const Lense &other) const;
    bool testIntersect(const float2 &pos, const float2 &dims) const;
    bool inside(float2 p) const;
    // check if point is insde Axis Aligned Bounding Box
    bool insideAABB(float2 p) const;
    void move(const float2 &p);
    void updateMesh();
    void cleanup();
};

struct Arrow final {
    Arrow();
    Arrow(float2 start, float len_in, bool virtual_in, float inf_sign=-1.0);
    // constructor for an arrow infinitely far away
    Arrow(float len_in, float inf_sign = -1.0);
    void putToMesh(Mesh &m, const float3 &color);
    void castRays(std::vector<ray2> &rays, const Lense &l);
    void move(float dx);
    void resize(float new_len);
    bool inside(const float2 &pos);
    
    seg2 v;
    float len;
    float infinity_sign = -1.0f;
    bool at_infinity = false;
    bool virtual_img = false;
};

void TW_CALL arrowSetX(const void *val, void *client_data);
void TW_CALL arrowGetX(void *val, void *client_data);
void TW_CALL arrowGetLen(void *val, void *client_data);
void TW_CALL arrowSetLen(const void *val, void *client_data);
void TW_CALL arrowSetInfinite(const void *val, void *client_data); 
void TW_CALL arrowGetInfinite(void *val, void *client_data); 

