#include <src/renderer.h>

#include <src/shader.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include <glfw3.h>

#include <AntTweakBar.h>

const char* WINDOWNAME = "Lenses";

#define DEBUGGING
#undef DEBUGGING

const float3 bg_color(0.4f, 0.4f, 0.6f); 

Renderer::Renderer(){
    width = 640 / 4 * 5;
    height = 480 / 4 * 5;
    initGL();
    initTweak();
    std::cout << "before initing scene\n";
    scene.init();
    std::cout << "finished initing in renderer\n";
}

Renderer::~Renderer(){
    TwDeleteAllBars();
    TwTerminate();
    glfwTerminate();
}

void Renderer::run(){
    glClearColor(bg_color.x, bg_color.y, bg_color.z, 0.0f);

    bool updated = false;
    
    #ifdef DEBUGGING
	const int max_cycles = 1;
	int cycle_counter = 0;
    #endif
    
    running = true;
    do {
	input();
	if (!updated) {
	    scene.update(inp);
	    updated = true;
	}
	updated = false;
	render();
	#ifdef DEBUGGING
	    cycle_counter++;
	    running = cycle_counter < max_cycles;
	#endif
    } while (running);
}

void Renderer::input(){
    double tmpx, tmpy;
    int lmb_state, rmb_state;

    glfwPollEvents();
    glfwGetCursorPos(window, &tmpx, &tmpy);
    lmb_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    rmb_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
    running = glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
        glfwWindowShouldClose(window) == 0;

    inp.prev_mousepos = inp.mousepos;
    inp.prev_lmb = inp.lmb_down;
    inp.prev_rmb = inp.rmb_down;
    
    float wh_ratio = (float)height / (float)width;
    inp.mousepos.x = (float) tmpx;
    inp.mousepos.y = (float) tmpy;
    inp.mousepos.x /= width;
    inp.mousepos.y /= height;
    inp.mousepos -= 0.5f;
    inp.mousepos *= 2.0f;
    inp.mousepos.y = -inp.mousepos.y;
    inp.mousepos /= scene.scale;
    inp.mousepos.y *= wh_ratio;
    inp.mousepos.y -= scene.yshift;

    inp.lmb_down = lmb_state == GLFW_PRESS;
    inp.rmb_down = rmb_state == GLFW_PRESS;
    // inp.cursor_inside = glfwGetWindowAttrib(window, GLFW_HOVERED);
}

void Renderer::render(){
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(programID);

    glUniform1f(uni_widthID, (float) width);
    glUniform1f(uni_heightID, (float) height);
    glUniform1f(uni_scalingID, (float) scene.scale);
    glUniform1f(uni_yshiftID, (float) scene.yshift);

    scene.draw();

    TwDraw();
    glfwSwapBuffers(window);
}

void Renderer::initGL(){
    int init_res = glfwInit();
    if (init_res != 1){
        fprintf(stderr, "Failed to initialize GLFW\n");
        return;
    }
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_RESIZABLE,GL_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    printf("1: %x\n", glGetError());

    window = glfwCreateWindow(width, height, WINDOWNAME, NULL, NULL);
    printf("2: %x\n", glGetError());
    if (window == NULL){
        fprintf(stderr, "Failed to create GLFW window\n");
        return;
    }
    glfwMakeContextCurrent(window);
    printf("3: %x\n", glGetError());

    glewExperimental = true; 
    if (glewInit() != GLEW_OK){
        fprintf(stderr, "Failed to init GLEW\n");
        return;
    }
    printf("4: %x\n", glGetError());
    glDisable(GL_DEPTH_TEST);
    // glEnable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    // glEnable(GL_ALPHA);
    // glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    printf("5: %x\n", glGetError());
	
    programID = loadShaders("vert.glsl", "frag.glsl");
    printf("6: %x\n", glGetError());
    uni_widthID = glGetUniformLocation(programID, "uni_width");
    uni_heightID = glGetUniformLocation(programID, "uni_height");
    uni_scalingID = glGetUniformLocation(programID, "uni_scaling");
    uni_yshiftID = glGetUniformLocation(programID, "uni_yshift");

    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_FALSE);
}

void TW_CALL myButtonCallback(void *data) {
    std::cout << "button pressed\n";
}

void Renderer::initTweak(){
    TwInit(TW_OPENGL_CORE, NULL);
    TwWindowSize(width, height);
    bar = TwNewBar("controls");
    TwDefine(" controls size='200 40'");
    TwDefine(" GLOBAL help='blablabla1' ");
    TwAddButton(bar, "add something", myButtonCallback, NULL,
		" label='make something happen' ");
    TwDefine("controls visible=false");

    glfwSetCursorPosCallback(window,
			     (GLFWcursorposfun) TwEventMousePosGLFW3);
    glfwSetMouseButtonCallback(window,
			       (GLFWmousebuttonfun) TwEventMouseButtonGLFW3);
    glfwSetKeyCallback(window, (GLFWkeyfun) TwEventKeyGLFW3);
    glfwSetCharCallback(window,(GLFWcharfun) TwEventCharGLFW3);
}


