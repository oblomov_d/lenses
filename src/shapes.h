#pragma once

#include <cstdlib>
#include <iostream>
#include <vector>

#include <src/vecmath.h>
#include <src/mesh.h>


// 2d segment
struct seg2 final {
    seg2() : a(0.0f), b(0.0f) {}
    seg2(float2 s, float2 e) : a(s), b(e) {} 
    seg2(float ax, float ay, float bx, float by) : a(ax, ay), b(bx, by) {}
    seg2(float2 s, float2 dir, float l) : a(s), b(s + dir*l) {}

    void putToMesh(Mesh &m, const float3 &color,
		   float thickness = 0.008) const;
    
    float2 a, b;
};

std::ostream & operator << (std::ostream &os, const seg2 &s);

float2 leftHandNormal(const seg2 &s);

void putToMesh(const seg2 &s, Mesh &m, const float3 &color,
	       float thickness = 0.008);
void putArrowToMesh(const seg2 &s, Mesh &m, const float3 &color,
	       float thickness = 0.008);

struct ray2 final {
    ray2() {}
    ray2(float2 s, float2 d) : pos(s), dir(normalize(d)) {}
    ray2(float sx, float sy, float dx, float dy); 
    ray2(seg2 seg) : pos(seg.a), dir(normalize(seg.b-seg.a)) {}

    float2 pos, dir;
};

std::ostream & operator << (std::ostream &os, const ray2 &r);
ray2 randomRay2();
bool refract(float2 r, float2 normal, float refr_c, float2 &res);
void reflect(float2 r, float2 normal, float2 &res);

struct line2 final {
    line2(float a, float b, float c) : dir(a, b), offs(c) {}
    line2(const ray2 &r){
	dir = float2(r.dir.y, -r.dir.x);
	offs = - dot(dir, r.pos);
    }

    float dist(const float2 &p);

    float2 dir;
    float offs;
};

struct RayPoint final {
    RayPoint() {}
    RayPoint(float t_in, float2 p_in) : t(t_in), p(p_in) {}
    RayPoint(float t_in, float2 p_in, float2 n_in)
	: t(t_in), p(p_in), n(n_in) {}
    float t = -1;
    float2 p;
    float2 n;
};

std::ostream & operator << (std::ostream &os, const RayPoint &rp);

struct IntersectRes final {
    IntersectRes() {}
    
    bool intersect = false;
    RayPoint pt;
};

std::ostream & operator << (std::ostream &os, const IntersectRes &ir); 

struct shape {
    virtual ~shape() {} 

    virtual void printType() const {}
    virtual IntersectRes intersect(const ray2 &r) const = 0;
    virtual void putToMesh(Mesh &m, const float3 &color) const = 0;
    virtual void move(float2 ofs) {}
    virtual bool inside(float2 p) const { return false; }
};


struct EmptyShape : shape {
    virtual IntersectRes intersect(const ray2 &r) const {
	IntersectRes res;
	res.intersect = false;
	return res;
    }
    virtual void putToMesh(Mesh &m, const float3 &color) const { }
};

struct ShapeUnion : shape {
    ShapeUnion();
    ~ShapeUnion();
    
    shape *a, *b;

    virtual void printType() const; 
    virtual IntersectRes intersect(const ray2 &r) const;
    virtual void putToMesh(Mesh &m, const float3 &color) const;
    virtual void move(float2 ofs);
    virtual bool inside(float2 p) const; 
};

struct ShapeInt : shape {
    ShapeInt();
    ~ShapeInt();
    
    shape *a = nullptr, *b = nullptr;

    virtual void printType() const; 
    virtual IntersectRes intersect(const ray2 &r) const;
    virtual void putToMesh(Mesh &m, const float3 &color) const;
    virtual void move(float2 ofs);
    virtual bool inside(float2 p) const; 
};

ShapeInt* makeBiconvex(float2 p, float w, float h, float r);
ShapeInt* makePlanoConvex(float2 p, float w, float h, float r); 

struct ShapeSub : shape {
    ShapeSub();
    ~ShapeSub();
    
    shape *pos = nullptr, *neg = nullptr;

    virtual void printType() const; 
    virtual IntersectRes intersect(const ray2 &r) const;
    virtual void putToMesh(Mesh &m, const float3 &color) const;
    virtual void move(float2 ofs);
    virtual bool inside(float2 p) const; 
};

ShapeSub* makePlanoConcave(float2 p, float d, float w, float h, float r);
ShapeSub* makeBiconcave(float2 p, float d, float w, float h, float r);
ShapeSub* makeMeniscus(float2 p, float d, float w, float h,
		       float r1, float r2);

struct circle : shape {
    circle(float2 center, float rad) : pos(center), r(rad) {}
    circle(float cx, float cy, float rad) : pos(cx, cy), r(rad) {}
    ~circle() {
	// std::cout << "destroying circle(" << pos << ", " << r << ")\n";
    }

    float2 pos;
    float r;

    virtual void printType() const;
    virtual IntersectRes intersect(const ray2 &r) const;
    virtual void putToMesh(Mesh &m, const float3 &color) const;
    virtual void move(float2 ofs);
    virtual bool inside(float2 p) const;
};

// assumes clockwise order of vertices
struct box : shape {
    box(float2 a_in, float2 b_in, float2 c_in, float2 d_in) {
	v[0] = a_in;
	v[1] = b_in;
	v[2] = c_in;
	v[3] = d_in;
	initEdges();
    }	
    box(float2 center, float w, float h) {
	float w2 = w * 0.5;
	float h2 = h * 0.5;
	v[0] = center - float2(w2,  h2);
	v[1] = center - float2(w2, -h2);
	v[2] = center + float2(w2,  h2);
	v[3] = center + float2(w2, -h2);
	initEdges();
    } 
    box(float cx, float cy, float w, float h) {
	float2 center(cx, cy);
	v[0] = center - float2(w, h);
	v[1] = center - float2(w, -h);
	v[2] = center + float2(w, h);
	v[3] = center + float2(w, -h);
	initEdges();
    } 

    void initEdges() {
	edges[0] = seg2(v[0], v[1]);
	edges[1] = seg2(v[1], v[2]);
	edges[2] = seg2(v[2], v[3]);
	edges[3] = seg2(v[3], v[0]);
    }

    ~box() {
    // 	std::cout << "destroying box(" << v[0] << ", " << v[1] << ", " <<
    // 	    v[2] << ", " << v[3] << ")\n";
    }
     
    float2 v[4];
    seg2 edges[4];

    virtual void printType() const;
    virtual IntersectRes intersect(const ray2 &r) const;
    virtual void putToMesh(Mesh &m, const float3 &color) const;
    virtual void move(float2 ofs);
    virtual bool inside(float2 p) const;
};

bool intersect(const ray2 &r1, const ray2 &r2, float2 &res, float &t);
bool intersect(const ray2 &r1, const ray2 &r2, float2 &res);
bool intersect(const ray2 &r1, const seg2 &s2, float2 &res, float &t);
bool intersect(const ray2 &r1, const seg2 &s2, float2 &res);
bool intersect(const seg2 &s1, const ray2 &r2, float2 &res, float &t);
bool intersect(const seg2 &s1, const ray2 &r2, float2 &res);
bool intersect(const seg2 &s1, const seg2 &s2, float2 &res, float &t);
bool intersect(const seg2 &s1, const seg2 &s2, float2 &res);
bool intersect(const float2 &s1a, const float2 &s1b,
	       const float2 &s2a, const float2 &s2b, float2 &res);
bool intersect(const float2 &s1a, const float2 &s1b,
	       const float2 &s2a, const float2 &s2b, float2 &res,
	       float &t);

bool intersect(const ray2 &r1, const circle &c2, float2 &res1, float2 &res2,
	       float &t1, float &t2);
bool intersect(const ray2 &r1, const circle &c2, float2 &res1, float2 &res2);

bool inside(const triangle &t, const float2 &p);

bool triangleIntersectCheck(const triangle &a, const triangle &b);

void intersectTriangleArrays(triangle *a, triangle *b,
			     int a_cnt, int b_cnt,
			     Mesh &m, const float3 &new_color);

void intersectTriangles(triangle &a, triangle &b,
			Mesh &m, const float3 &new_color);

void subtractTriangleArrays(triangle *a, triangle *b,
			    int a_cnt, int b_cnt,
			    Mesh &m, const float3 &new_color);

void subtractTriangles(triangle &a, triangle &b, Mesh &m,
		       const float3 &new_color);

