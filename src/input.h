#pragma once

#include <GL/glew.h>
#include <GL/gl.h>

#include <glfw3.h>

#include <src/vecmath.h>

struct Input {
    float2 mousepos, prev_mousepos;
    bool cursor_inside;
    bool lmb_down, prev_lmb;
    bool rmb_down, prev_rmb;
};

