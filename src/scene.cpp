#include <src/scene.h>

#include <src/input.h>
#include <src/shapes.h>


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#define TRIANGLE_DEBUG
#undef TRIANGLE_DEBUG

#define NORMAL_DEBUG
#undef NORMAL_DEBUG


const char* const LENSE_NAMES[] =
    {"empty", "pos_meniscus", "biconvex",
     "planoconvex", "planoconcave", "neg_meniscus", "biconcave"};

extern Mesh debug_mesh;

const int DEFAULT_LENS_CNT = 5;

const float ray_extension = 500.0f;
const float ray_thickness = 0.04f;
const float default_lense_height = 20.0f;
const float default_lense_width = 1.5f;
const float default_lense_thickness = 0.8;
const float default_lense_radius = 60.0f;
const float default_biconcave_radius = 100.f;
const float default_neg_meniscus_r1 = 100.0f;
const float default_neg_meniscus_r2 = 70.0f;
const float default_pos_meniscus_r1 = 90.0f;
const float default_pos_meniscus_r2 = 120.0f;

const float default_refr_c = 1.5f;
const float3 arrow_color(1.0, 0.6, 0.6);
const float3 arrow_im_color(0.3, 0.6, 1.0);
const float3 ray_palette[] =
    {{0.0f, 0.0f, 0.0f}, {0.3f, 0.3f, 0.3f},
     {0.4f, 0.6f, 0.6f}
    };

char filename_buf[128] = "out.cfg";


#ifdef TRIANGLE_DEBUG
Mesh test_mesh;
triangle tri_a, tri_b;
int selected_tri_a, selected_tri_b;
bool test_updated = false;
float3 tri_color1(0.5, 0.5, 0.5);
float3 tri_color2(0.1, 0.5, 0.5);
float3 tri_color3(0.5, 0.1, 0.5);
#endif

#ifdef NORMAL_DEBUG
Mesh normal_mesh;
float3 normal_color(0.8, 0.0, 0.8);

#endif

//implemented in the end of file
void TW_CALL setFilenameString(const void *value, void *client_data);
void TW_CALL getFilenameString(void *value, void *client_data);
void TW_CALL saveSceneCB(void *client_data);
void TW_CALL loadSceneCB(void *client_data);
bool parseInt(const char* cstr, int &res);
bool parseFloat(const char* cstr, float &res);
void initLenseFromEnum(Lense &l, LENSE_TYPE ltype, float2 tmp_pos,
		       float focal_len = 0);


void Scene::init(int lcnt, LENSE_TYPE *ltypes, float *lxs, float *lfls,
		 float ax, float alen, bool at_inf, float inf_sign,
		 bool dticks, bool darrows, bool drays){
    initialized = true;

    debug_mesh.init();

    seg_mesh.init();
    arrow_mesh.init();
    axis_mesh.init();
    ticks_mesh.init();

    display_ticks = dticks;
    display_arrows = darrows;
    display_rays = drays;
    
    float3 axis_color(0.05f, 0.05f, 0.1f);
    float axis_thick = 0.04;
    seg2 axis_seg;

    float axis_sx = -1.0f / scale;
    float axis_ex =  1.0f / scale;
    float axis_h1 = notch_len * 0.5;
    float axis_h2 = notch_len * 0.7;
    float axis_h3 = notch_len * 0.9;
    float axis_origin_h = notch_len * 1.0;

    // the horizontal axis
    axis_seg.a = float2(axis_sx, 0.0f);
    axis_seg.b = float2(axis_ex, 0.0f);
    axis_seg.putToMesh(axis_mesh, axis_color, axis_thick);

    axis_seg.a.y = -axis_origin_h;
    axis_seg.b.y =  axis_origin_h;
    axis_seg.a.x = 0.0f;
    axis_seg.b.x = 0.0f;
    axis_seg.putToMesh(ticks_mesh, axis_color, axis_thick);
   
    int notch_cnt = 0;
    for (float x = notch_len; x < axis_ex; x += notch_len) {
	notch_cnt++;
	if (notch_cnt % 10 == 0) {
	    axis_seg.a.y = -axis_h3;
	    axis_seg.b.y =  axis_h3;
	} else if (notch_cnt % 5 == 0) {
	    axis_seg.a.y = -axis_h2;
	    axis_seg.b.y =  axis_h2;
	} else {
	    axis_seg.a.y = -axis_h1;
	    axis_seg.b.y =  axis_h1;
	}
	axis_seg.a.x = x;
	axis_seg.b.x = x;
	axis_seg.putToMesh(ticks_mesh, axis_color, axis_thick);
    }
    notch_cnt = 0;
    for (float x = -notch_len; x > axis_sx; x -= notch_len) {
	notch_cnt++;
	if (notch_cnt % 5 == 0) {
	    axis_seg.a.y = -axis_h2;
	    axis_seg.b.y =  axis_h2;
	} else {
	    axis_seg.a.y = -axis_h1;
	    axis_seg.b.y =  axis_h1;
	}
	axis_seg.a.x = x;
	axis_seg.b.x = x;
	axis_seg.putToMesh(ticks_mesh, axis_color, axis_thick);
    }
    
    arrow = Arrow(float2(ax, 0), alen, false, inf_sign);
    arrow.at_infinity = at_inf;

    lense_cnt = lcnt;
    lenses = new Lense[lense_cnt];
    target_types = new LENSE_TYPE[lense_cnt];

    if (ltypes != nullptr) {
	for (int i = 0; i < lense_cnt; i++) {
	    float2 tmp_pos(lxs[i], 0); 
	    initLenseFromEnum(lenses[i], ltypes[i], tmp_pos, lfls[i]);
	    target_types[i] = ltypes[i];
	}
    } else {
	lenses[0].initBiconvex(float2(0.0, 0.0), default_refr_c,
			    default_lense_width, default_lense_height,
			    default_lense_radius);

	for (int i = 1; i < 5; i++) {
	    lenses[i].initEmpty(float2(i * 5, 0.0));
	}
	}

    initBars();


    #ifdef NORMAL_DEBUG
    normal_mesh.init();
    #endif
    
    #ifdef TRIANGLE_DEBUG
    test_mesh.init();
    tri_a.a = float2(0.0, 0.0);
    tri_a.b = float2(0.5, 0.5);
    tri_a.c = float2(0.5, 0.0);

    tri_b.a = float2(1.0, 0.0);
    tri_b.b = float2(1.0, 0.5);
    tri_b.c = float2(1.5, 0.0);

    test_mesh.addTriangle(tri_a, tri_color1); 
    test_mesh.addTriangle(tri_b, tri_color2); 
    #endif

}

void Scene::saveToFile(char *filename) {
    std::fstream out_file;
    out_file.open(filename, std::ios::out);
    if (out_file.is_open()) {
	out_file << lense_cnt << "\n";
	
	for (int i = 0; i < lense_cnt; i++) {
	    const char* lname = LENSE_NAMES[int(lenses[i].type)];
	    float lx = lenses[i].pos.x;
	    float lfl = lenses[i].params.focal_len; 

	    out_file << "lense " << lname << " " << lx << " " << lfl << "\n";
	}
	float ax = arrow.v.a.x;
	float alen = arrow.len;
	bool at_inf = arrow.at_infinity;
	float inf_sign = arrow.infinity_sign;

	out_file << "arrow " << ax << " " << alen << " " << at_inf <<
	    " " << inf_sign << "\n";
	out_file << display_ticks << " " << display_arrows << " " <<
	    display_rays << "\n";
    } else {
	std::cerr << "failed to open the output file with name \"" <<
	    filename << "\"\n";
	return;
    }
    std::cout << "finished saving scene to " << filename << "\n";
}

void fileInitCleanup(LENSE_TYPE *ltypes, float* lxs, float *lfls) {
    if (ltypes != nullptr) {
	delete [] ltypes;
	delete [] lxs;
	delete [] lfls;
    }
}

void Scene::initFromFile(char *filename) {
    std::fstream in_file;
    bool succ = true;
    int line_fail = 0;
    in_file.open(filename, std::ios::in);


    int lcnt;
    LENSE_TYPE *ltypes = nullptr;
    float *lxs = nullptr;
    float *lfls = nullptr;
    float ax;
    float alen;
    bool at_inf;
    float inf_sign;
    bool dticks, darrows, drays;
    if (in_file.is_open()) {
	std::string line;
	std::getline(in_file, line);
	std::cout << line << "\n";

	if (!parseInt(line.c_str(), lcnt) || lcnt < 0 ||
	    lcnt > DEFAULT_LENS_CNT) {
	    succ = false;
	    std::cerr << "error in file " << filename << " at line " <<
		line_fail << "\n";
	    return;
	}
	line_fail++;
	ltypes = new LENSE_TYPE[lcnt];
	lxs = new float[lcnt];
	lfls = new float[lcnt];

	for (int i = 0; i < lcnt; i++) {
	    std::string slense, slname, slx, slfl; 
	    std::getline(in_file, line);
	    std::stringstream ss(line);
	    std::getline(ss, slense, ' '); 
	    std::getline(ss, slname, ' ');
	    std::getline(ss, slx, ' ');
	    std::getline(ss, slfl, ' ');

	    bool subsucc = true;
	    subsucc &= strcmp(slense.c_str(), "lense") == 0; 
	    subsucc &= ltypeFromString(slname.c_str(), ltypes[i]);
	    subsucc &= parseFloat(slx.c_str(), lxs[i]);
	    subsucc &= parseFloat(slfl.c_str(), lfls[i]);
	    if (!subsucc) {
		std::cerr << "error in file " << filename << " at line " <<
		    line_fail << "\n";
		succ = false;
		fileInitCleanup(ltypes, lxs, lfls);
		return;
	    }
	    line_fail++;
	}
	int tmp_at_inf;
	std::string sarrow, sax, salen, sat_inf, sinf_sign; 
	std::getline(in_file, line);
	std::stringstream ss(line);
	std::getline(ss, sarrow, ' ');
	std::getline(ss, sax, ' ');
	std::getline(ss, salen, ' ');
	std::getline(ss, sat_inf, ' ');
	std::getline(ss, sinf_sign, ' ');
	succ &= strcmp(sarrow.c_str(), "arrow") == 0;
	succ &= parseFloat(sax.c_str(), ax);
	succ &= parseFloat(salen.c_str(), alen);
	succ &= parseInt(sat_inf.c_str(), tmp_at_inf);
	at_inf = (bool)tmp_at_inf;
	succ &= parseFloat(sinf_sign.c_str(), inf_sign);
	if (!succ) {
	    std::cerr << "error in file " << filename << " at line " <<
		line_fail << "\n";
	    fileInitCleanup(ltypes, lxs, lfls);
	    return;
	}
	line_fail++;

	int t_dticks, t_darrows, t_drays;
	std::string sdticks, sdarrows, sdrays; 
	std::getline(in_file, line);
	std::stringstream bss(line);
	std::getline(bss, sdticks, ' ');
	std::getline(bss, sdarrows, ' ');
	std::getline(bss, sdrays, ' ');
	succ &= parseInt(sdticks.c_str(), t_dticks);
	dticks = (bool) t_dticks;
	succ &= parseInt(sdarrows.c_str(), t_darrows);
	darrows = (bool) t_darrows;
	succ &= parseInt(sdrays.c_str(), t_drays);
	drays = (bool) t_drays;
	if (!succ) {
	    std::cerr << "error in file " << filename << " at line " <<
		line_fail << "\n";
	    fileInitCleanup(ltypes, lxs, lfls);
	    return;
	}
	line_fail++;
    } else {
	std::cerr << "failed to open the input file with name \"" <<
	    filename << "\"\n";
	return;
    }
    if (succ) {
	cleanup();

	init(lcnt, ltypes, lxs, lfls, ax, alen, at_inf, inf_sign,
	     dticks, darrows, drays);
    }
    
    fileInitCleanup(ltypes, lxs, lfls);
    std::cout << "finished loading scene from " << filename << "\n";
}

void Scene::initBars() {
    char tmp_cstr[256];
    
    TwEnumVal Lenses[] =
	{{EMPTY, "None"},
	 {POS_MENISCUS, "Positive Meniscus"},
	 {BICONVEX, "Biconvex"}, {PLANOCONVEX, "Planoconvex"},
	 {PLANOCONCAVE, "Planoconcave"}, {NEG_MENISCUS, "Negative Meniscus"},
	 {BICONCAVE, "Biconcave"}};
    TwType LenseTwType = TwDefineEnum("LenseType", Lenses, 7);
    
    editor = TwNewBar("editor");
    TwAddButton(editor, "labelPlaceholder", NULL, NULL,
		"label='Select a lens to start editing'");
    TwDefine("editor position='5 5'");
    TwDefine("editor size='200 140'");
    TwDefine("editor visible=true");
    TwDefine("editor color='100 156 156'");

    selector = TwNewBar("selector");
    TwDefine("selector position='230 5'");
    TwDefine("selector size='250 140'");
    TwDefine("selector iconalign=horizontal");
    TwDefine("selector color='100 156 156'");

    settings = TwNewBar("settings");
    TwDefine("settings position='505 5'");
    TwDefine("settings size='290 175'");
    TwDefine("settings color='100 156 156'");
    TwAddVarCB(settings, "Length", TW_TYPE_FLOAT, arrowSetLen,
	       arrowGetLen, (void*) &arrow,
	       "label='Arrow length', step='0.5', min='-7.0', max='7.0'");
    TwAddVarCB(settings, "ArrowX", TW_TYPE_FLOAT, arrowSetX, arrowGetX,
	       (void*) &arrow,
	       "label='Arrow x', step='0.5', min='-100.0', max='100.0'");
    TwAddVarCB(settings, "Infinity", TW_TYPE_BOOLCPP, arrowSetInfinite,
	       arrowGetInfinite, (void*) &arrow,
	       "label='Toggle arrow at infinity'");
    TwAddVarRW(settings, "DrawTicks", TW_TYPE_BOOLCPP, &display_ticks,
	       "label='Toggle axis drawing'");
    TwAddVarRW(settings, "DrawRays", TW_TYPE_BOOLCPP, &display_rays,
	       "label='Toggle ray drawing'");
    TwAddVarRW(settings, "DrawArrows", TW_TYPE_BOOLCPP, &display_arrows,
	       "label='Toggle arrow drawing'");

    TwAddVarCB(settings, "Filename", TW_TYPE_CSSTRING(sizeof(filename_buf)),
	       setFilenameString, getFilenameString, filename_buf, "");
    TwAddButton(settings, "Save", saveSceneCB, this, "");
    TwAddButton(settings, "Load", loadSceneCB, this, "");

    for (int i = 0; i < lense_cnt; i++) {
	target_types[i] = lenses[i].type;
	snprintf(tmp_cstr, 256, "Lense %d", i+1);
	TwAddVarRW(selector, tmp_cstr, LenseTwType, &target_types[i], NULL);
    }
}

void Scene::updateBars() {
    if (selected_lense == -1) {
	return;
    }
    if (selected_lense == last_selected_lense) {
	return;
    }
    TwDeleteBar(editor);
    editor = TwNewBar("editor");
    TwDefine("editor position='5 5'");
    TwDefine("editor size='200 140'");
    TwDefine("editor color='100 156 156'");
    // TwAddVarCB(editor, "RefrInd", TW_TYPE_FLOAT, NULL,
    // 	       lenseGetRefrC,
    // 	       (void*) &lenses[selected_lense].params,
    // 	       "label='Refractive Index', step='0.05', min='0.05', "
    // 	       "max='4.0'");

    TwAddVarCB(editor, "Focal", TW_TYPE_FLOAT, lenseSetF, lenseGetF,
	       (void*) &lenses[selected_lense].params,
	       "label='Focal length', step='0.5', min='-100.0', "
	       "max='100.0'");
    TwAddVarCB(editor, "posX", TW_TYPE_FLOAT, lenseSetX, lenseGetX,
	       (void*) &lenses[selected_lense],
	       "label='Lens x', step='0.5', min='-100', max='100'");

}

void Scene::updateSelectedLense() {
    int ind = last_selected_lense;
    if (ind == -1)
	return;
}

void Scene::update(const Input &inp){
    debug_mesh.clear();
    
    seg_color_inds.clear();
    segs.clear();
    seg_mesh.clear();

    arrows.clear();
    arrow_mesh.clear();

    // updating the lense types
    for (int i = 0; i < lense_cnt; i++) {
	if (lenses[i].type != target_types[i]) {
	    lenses[i].cleanup();
	    float2 tmp_pos = lenses[i].pos;
	    for (int j = 0; j < i; j++) {
		if (lenses[j].type != LENSE_TYPE::EMPTY) {
		    if (lenses[j].pos.x > tmp_pos.x) {
			tmp_pos.x = lenses[j].pos.x + lenses[j].dims.x;
		    }
		}
	    }
	    initLenseFromEnum(lenses[i], target_types[i], tmp_pos);
	}
    }

    #ifdef TRIANGLE_DEBUG
    if (inp.lmb_down) {
	float2 *a_v = &tri_a.a;
	float2 *b_v = &tri_b.a;
	float min_a = 1e-1;
	float min_b = 1e-1;
	float tmp_len;
	if (!inp.prev_lmb) {
	    for (int i = 0; i < 3; i++) {
		tmp_len = length(a_v[i] - inp.mousepos);
		if (tmp_len < min_a) {
		    selected_tri_a = i;
		    min_a = tmp_len;
		} 
		tmp_len = length(b_v[i] - inp.mousepos);
		if (tmp_len < min_b) {
		    selected_tri_b = i;
		    min_b = tmp_len;
		} 
	    }
	    if (min_a < min_b) {
		selected_tri_b = -1;
	    } else {
		selected_tri_a = -1;
	    }
	}
	float2 mouse_dif = inp.mousepos - inp.prev_mousepos;
	if (selected_tri_a != -1) {
	    a_v[selected_tri_a] += mouse_dif;
	}
	if (selected_tri_b != -1) {
	    b_v[selected_tri_b] += mouse_dif;
	}
    } else {
	selected_tri_a = -1;
	selected_tri_b = -1;
    }
    test_updated = selected_tri_a != -1 || selected_tri_b != -1;
    #endif

    #ifdef NORMAL_DEBUG
    normal_mesh.clear();
    #endif

    // processing movement with mouse
    if (selected_lense != -1) {
	last_selected_lense = selected_lense;
    }
    if (inp.lmb_down) {
	float2 move_d(inp.mousepos.x - inp.prev_mousepos.x, 0.0f);
	if (!inp.prev_lmb) {
	    if (arrow.inside(inp.mousepos)) {
		selected_arrow = true;
	    } 
	    
	    for (int i = lense_cnt - 1; i >= 0; i--) {
		if (lenses[i].sh->inside(inp.mousepos)) {
		    selected_lense = i;
		    break;
		}
	    }
	}
	if (selected_lense != -1) {
	    lenses[selected_lense].move(move_d);
	}
	if (selected_arrow) {
	    arrow.move(move_d.x);
	}
    } else {
	selected_arrow = false;
	selected_lense = -1;
    }

    // prevent lense collisions
    for (int i = 0; i < lense_cnt; i++) {
	for (int j = i+1; j < lense_cnt; j++) {
	    if (lenses[j].testIntersect(lenses[i])) {
		float dx = lenses[i].pos.x - lenses[j].pos.x +
		    lenses[i].dims.x + lenses[j].dims.x;
		float2 displace(dx, 0);
		lenses[j].move(displace);
	    }
	}
    }
    
    updateBars();
    updateSelectedLense();

    // doing the image calculation

    Arrow cur_ar = arrow;
    Arrow new_ar;
    float2 prev_b;
    float2 prev_lense_pos;
    float neg_inf = -ray_extension;
    if (cur_ar.at_infinity) {
	cur_ar.move(neg_inf);
    }
    prev_b = cur_ar.at_infinity ? float2(neg_inf, cur_ar.len) : cur_ar.v.b;
    bool prev_inf = false;
    bool in_focus = false;
    bool dot_case = false;
    float2 prev_inf_dir;
    float2 lense_top_p;
    float2 prev_inf_ray_dir;
    float prev_dot_len = 0.0;
    
    for (int lense_ind = 0; lense_ind < lense_cnt; lense_ind++) {
	const Lense &lense = lenses[lense_ind];
	if (lense.type == LENSE_TYPE::EMPTY)
	    continue;

	float l0 = lense.pos.x - cur_ar.v.a.x;
	const float &f = lense.params.focal_len;
	float2 focal_p = lense.getFocalPoint();
	lense_top_p = float2(lense.pos.x, cur_ar.len);

	float prev_l, cur_l, new_len;
	if (prev_inf) {
	    dot_case = true;
	    prev_l = cur_ar.v.a.x - prev_lense_pos.x;
	    cur_l = lense.pos.x - cur_ar.v.a.x;
	    new_len = -prev_b.y * cur_l / prev_l;
	    prev_dot_len = new_len;
	} else if (dot_case) {
	    prev_l = cur_ar.v.a.x - prev_lense_pos.x;
	    cur_l = lense.pos.x - cur_ar.v.a.x;
	    new_len = -prev_dot_len * cur_l / prev_l;
	}
	
	if (cur_ar.at_infinity) {
	    float2 new_start = focal_p;
	    bool virtual_img = cur_ar.infinity_sign * f > 0;
	    new_ar = Arrow(new_start, 0.0f, virtual_img);
	    prev_inf_ray_dir = normalize(focal_p -
					 float2(lense.pos.x, cur_ar.v.b.y));
	} else {
	    float li, d;
	    in_focus = fabs(l0 - f) < LENSE_EPS;
	    if (in_focus) {
		if (dot_case) {
		    float2 new_lens_pos(lense.pos.x, new_len);
		    addSeg(seg2(cur_ar.v.a, new_lens_pos), 0);
		    addSeg(seg2(cur_ar.v.a, lense.pos), 0);
		    
		    new_ar = Arrow(lense.pos, new_len, false);
		    new_ar.at_infinity = true;
		    dot_case = false;
		} else {
		    prev_inf_dir = normalize(lense.pos - cur_ar.v.b);
		    addSeg(seg2(cur_ar.v.b, lense_top_p), 0);
		    addSeg(seg2(cur_ar.v.b, lense.pos), 0);
		    prev_lense_pos = lense.pos;
		    break;
		}
	    } else {
		if (dot_case && (fabs(new_len) > lense.dims.y)) {
			float2 ray_lense_dir(lense.pos.x, new_len);
			ray_lense_dir -= cur_ar.v.a;
			ray_lense_dir = normalize(ray_lense_dir);
			addSeg(seg2(cur_ar.v.a,
				    cur_ar.v.a +
				    ray_extension * ray_lense_dir), 0);
			break;
		} else {
		    li = l0 * f / (l0 - f);
		    d = -li / l0;
		    bool virtual_img;
		    virtual_img = l0*li < 0;
		    float2 new_start = lense.pos;
		    new_start.x += li;
		    float scale_len = cur_ar.len * d;
		    new_ar = Arrow(new_start, scale_len, virtual_img);
		    if (dot_case) {
			lense_top_p = float2(lense.pos.x, new_len);
			addSeg(seg2(cur_ar.v.a, lense_top_p), 0);
			prev_inf_ray_dir = normalize(new_ar.v.b -
						     lense_top_p); 
		    }
		}
	    }
	}

	seg2 s_top1, s_top2, s_bs, s_center;
	if (cur_ar.at_infinity) {
	    s_top1 = seg2(cur_ar.v.b, lense_top_p);
	    s_top2 = seg2(lense_top_p, new_ar.v.b);
	    addSeg(s_top1, 0);
	    addSeg(s_top2, 0);
	    if (new_ar.virtual_img) {
		seg2 s_top2_ext, s_center_ext;
		float2 top2_p = s_top2.b +
		    normalize(s_top2.a - s_top2.b) * ray_extension;
		s_top2_ext = seg2(s_top2.a, top2_p);
		addSeg(s_top2_ext, 2);
	    }
	} else {
	    if (!new_ar.at_infinity) {
		s_top1 = seg2(cur_ar.v.b.x, cur_ar.v.b.y,
			    lense.pos.x, cur_ar.v.b.y);
		s_top2 = seg2(lense_top_p, new_ar.v.b);
		s_center = seg2(cur_ar.v.b, lense.pos);
		s_bs = seg2(cur_ar.v.b, new_ar.v.b);
		addSeg(s_center, 0);
		addSeg(s_bs, 0);
		addSeg(s_top1, 0);
		addSeg(s_top2, 0);

		if (new_ar.virtual_img) {
		    seg2 s_top2_ext, s_center_ext;
		    float2 top2_p = s_top2.b +
			normalize(s_top2.a - s_top2.b) * ray_extension;
		    s_top2_ext = seg2(s_top2.a, top2_p);
		    float2 center_p = s_center.b +
			normalize(s_center.b - s_center.a) * ray_extension; 
		    s_center_ext = seg2(s_center.b, center_p);
		    addSeg(s_top2_ext, 2);
		    addSeg(s_center_ext, 2);
		}
	    }
	}
	prev_lense_pos = lense.pos;
	prev_inf = cur_ar.at_infinity;
	prev_b = cur_ar.v.b;
	cur_ar = new_ar;

	arrows.push_back(new_ar);
    }

    // postprocessing, check if we need to cast rays to infinity
    if (in_focus) {
	float2 r1_p(prev_lense_pos + ray_extension * prev_inf_dir);
	float2 r2_p(lense_top_p + ray_extension * prev_inf_dir);
	addSeg(seg2(prev_lense_pos, r1_p), 1);
	addSeg(seg2(lense_top_p, r2_p), 1);
    }
    if (dot_case || prev_inf) {
	addSeg(seg2(cur_ar.v.b,
		    cur_ar.v.b + prev_inf_ray_dir * ray_extension), 0);
    } 
    if (cur_ar.at_infinity) {
	addSeg(seg2(cur_ar.v.b,
		    cur_ar.v.b + float2(1.0f, 0.0f)*ray_extension), 0);
    }

    #ifdef TRIANGLE_DEBUG
    if (test_updated) {
	test_mesh.clear();
	test_mesh.addTriangle(tri_a, tri_color1); 
	test_mesh.addTriangle(tri_b, tri_color2); 
	subtractTriangles(tri_a, tri_b, test_mesh, tri_color3);
	std::cout << "---\n";
    }
    #endif

    //put rays and arrows to meshes
    for (int i = 0; i < segs.size(); i++) {
	segs[i].putToMesh(seg_mesh, ray_palette[seg_color_inds[i]],
			  ray_thickness);
    }

    arrow.putToMesh(arrow_mesh, arrow_color);
    for (int i = 0; i < arrows.size(); i++) {
	if (arrows[i].virtual_img) {
	    arrows[i].putToMesh(arrow_mesh, arrow_im_color);
	} else {
	    arrows[i].putToMesh(arrow_mesh, arrow_color);
	}
    }
} 

//assumes renderer has enabled the right program
void Scene::draw() {
    for (int i = 0; i < lense_cnt; i++) {
	lenses[i].mesh.buffer();
    }
    for (int i = 0; i < lense_cnt; i++) {
	lenses[i].mesh.draw();
    }

    axis_mesh.buffer();
    axis_mesh.draw();

    if (display_ticks) {
	ticks_mesh.buffer();
	ticks_mesh.draw();
    }
    
    if (display_rays) {
	seg_mesh.buffer();
	seg_mesh.draw();
    }

    if (display_arrows) {
	arrow_mesh.buffer();
	arrow_mesh.draw();
    }
    
    debug_mesh.buffer();
    debug_mesh.draw();

    #ifdef NORMAL_DEBUG
    normal_mesh.buffer();
    normal_mesh.draw();
    #endif
    
    #ifdef TRIANGLE_DEBUG
    test_mesh.buffer();
    test_mesh.draw();
    #endif
}

void Scene::addSeg(const seg2 &s, int color_ind) {
    if (display_rays) {
	segs.push_back(s);
	seg_color_inds.push_back(color_ind);
    }
}

void Scene::cleanup() {
    if (initialized) {
	for (int i = 0; i < lense_cnt; i++){
	    lenses[i].cleanup();
	}
	
	delete [] lenses;
	delete [] target_types;
	initialized = false;
	TwDeleteAllBars();
    }
}

Scene::~Scene(){
    cleanup();
}

void TW_CALL setFilenameString(const void *value, void *client_data) {
    const char *src = (const char*) value;
    strncpy(filename_buf, src, sizeof(filename_buf));
    filename_buf[sizeof(filename_buf) - 1] = '\0';
}

void TW_CALL getFilenameString(void *value, void *client_data) {
    char *dest = (char*) value;
    strncpy(dest, filename_buf, sizeof(filename_buf));
    dest[sizeof(filename_buf)-1] = '\0';
}

void TW_CALL saveSceneCB(void *client_data) {
    Scene *s = (Scene*)client_data;
    s->saveToFile(filename_buf);
}

void TW_CALL loadSceneCB(void *client_data) {
    Scene *s = (Scene*) client_data;
    s->initFromFile(filename_buf);
}

bool parseInt(const char *cstr, int &res) {
    char *tail;
    errno = 0;
    long val = strtol(cstr, &tail, 10);
    if (errno != 0) {
	return false;
    }
    if (*tail != '\0')
	return false;
    res = (int)val;
    return true;
}

bool parseFloat(const char* cstr, float &res) {
    char *tail;
    errno = 0;
    double val = strtod(cstr, &tail);
    if (errno != 0) {
	return false;
    }
    if (*tail != '\0')
	return false;
    res = (float)val;
    return true;
}


void initLenseFromEnum(Lense &l, LENSE_TYPE ltype, float2 tmp_pos,
		       float focal_len) {
    switch (ltype) {
    case LENSE_TYPE::EMPTY:
	l.initEmpty(tmp_pos);
	break;
    case LENSE_TYPE::POS_MENISCUS:
	l.initMeniscus(tmp_pos, default_refr_c,
				default_lense_thickness,
				default_lense_width,
				default_lense_height,
				default_pos_meniscus_r1,
				default_pos_meniscus_r2);
	if (fabs(focal_len) > LENSE_EPS) {
	    l.params.focal_len = focal_len;
	} 
	break;
    case LENSE_TYPE::BICONVEX:
	l.initBiconvex(tmp_pos, default_refr_c,
				default_lense_width,
				default_lense_height,
				default_lense_radius);
	if (fabs(focal_len) > LENSE_EPS) {
	    l.params.focal_len = focal_len;
	} 
	break;
    case LENSE_TYPE::PLANOCONVEX:
	l.initPlanoConvex(tmp_pos, default_refr_c,
				    default_lense_width,
				    default_lense_height,
				    default_lense_radius);
	if (fabs(focal_len) > LENSE_EPS) {
	    l.params.focal_len = focal_len;
	} 
	break;
    case LENSE_TYPE::PLANOCONCAVE:
	l.initPlanoConcave(tmp_pos, default_refr_c,
				    default_lense_thickness,
				    default_lense_width,
				    default_lense_height,
				    default_lense_radius);
	if (fabs(focal_len) > LENSE_EPS) {
	    l.params.focal_len = focal_len;
	} 
	break;
    case LENSE_TYPE::NEG_MENISCUS:
	l.initMeniscus(tmp_pos, default_refr_c,
				default_lense_thickness,
				default_lense_width,
				default_lense_height,
				default_neg_meniscus_r1,
				default_neg_meniscus_r2);
	if (fabs(focal_len) > LENSE_EPS) {
	    l.params.focal_len = focal_len;
	} 
	break;
    case LENSE_TYPE::BICONCAVE:
	l.initBiconcave(tmp_pos, default_refr_c,
				default_lense_thickness,
				default_lense_width,
				default_lense_height,
				default_biconcave_radius);
	if (fabs(focal_len) > LENSE_EPS) {
	    l.params.focal_len = focal_len;
	} 
	break;
    default:
	break;
    }
}


