#pragma once

#include <iostream>

#include <cmath>

#define SQR(x) ((x)*(x))

const float LENSE_EPS = 1e-4;

struct float2 final {
    float2() : x(0), y(0) {}
    float2(float a, float b) : x(a), y(b) {}
    float2(float v) : x(v), y(v) {}
    
    float x, y;
};

struct float3 final {
    float3() : x(0), y(0), z(0) {}
    float3(float a, float b, float c) : x(a), y(b), z(c) {}
    float3(float v) : x(v), y(v), z(v) {}
    
    float x, y, z;
};

struct triangle final {
    triangle() : a(0), b(0), c(0) {}
    triangle(float2 a_in, float2 b_in, float2 c_in) : a(a_in), b(b_in),
						      c(c_in) {}
    
    float2 a, b, c;
};

std::ostream & operator << (std::ostream &os, const triangle &t); 
std::ostream & operator << (std::ostream &os, const float2 &p); 

static inline float clamp(float u, float a, float b) {
    return fmin(fmax(a, u), b);
}


static inline float2 operator * (const float2 &u, float v) {
    return float2(u.x * v, u.y * v); }

static inline float2 operator / (const float2 &u, float v) {
    return float2(u.x / v, u.y / v); }

static inline float2 operator * (float v, const float2 &u) {
    return float2(u.x * v, u.y * v); }

static inline float2 operator / (float v, const float2 &u) {
    return float2(v / u.x, v / u.y); }

static inline float2 operator + (const float2 &u, const float2 &v) {
    return float2(u.x + v.x, u.y + v.y); }

static inline float2 operator - (const float2 &u, const float2 &v) {
    return float2(u.x - v.x, u.y - v.y); }

static inline float2 operator * (const float2 &u, const float2 &v) {
    return float2(u.x * v.x, u.y * v.y); } 

static inline float2 operator / (const float2 &u, const float2 &v) {
    return float2(u.x / v.x, u.y / v.y); }

static inline float2 operator - (const float2 &v) {
    return float2(-v.x, -v.y); }

static inline float2 & operator += (float2 &u, const float2 &v) {
    u.x += v.x; u.y += v.y; return u; }

static inline float2 & operator -= (float2 &u, const float2 &v) {
    u.x -= v.x; u.y -= v.y; return u; }

static inline float2 & operator *= (float2 &u, const float2 &v) {
    u.x *= v.x; u.y *= v.y; return u; }

static inline float2 & operator /= (float2 &u, const float2 &v) {
    u.x /= v.x; u.y /= v.y; return u; }

static inline float2 & operator += (float2 &u, float v) {
    u.x += v; u.y += v; return u; }

static inline float2 & operator -= (float2 &u, float v) {
    u.x -= v; u.y -= v; return u; }

static inline float2 & operator *= (float2 &u, float v) {
    u.x *= v; u.y *= v; return u; }

static inline float2 & operator /= (float2 &u, float v) {
    u.x /= v; u.y /= v; return u; }

static inline float2 lerp(const float2 &u, const float2 &v, float t) {
    return u + t * (v - u); }

static inline float dot(const float2 &u, const float2 &v) {
    return u.x * v.x + u.y * v.y; }

static inline float dotsqr(const float2 &u) {
    return u.x * u.x + u.y * u.y;
}

static inline float skew(const float2 &u, const float2 &v) {
    return u.x * v.y - u.y * v.x; }

static inline float2 cross2d(const float2 &u, float zdir) {
    float2 res;
    res.x = -zdir * u.y;
    res.y = zdir * u.x;
    return res;
}

static inline float length(const float2 &u) {
    return sqrtf(SQR(u.x) + SQR(u.y)); }

static inline float2 normalize(const float2 &u){
    float len = length(u) + 1e-10;
    return float2(u.x / len, u.y / len);
}

static inline float2 min(const float2 &u, const float2 &v) {
    return float2(fmin(u.x, v.x), fmin(u.y, v.y));
}

static inline float2 max(const float2 &u, const float2 &v) {
    return float2(fmax(u.x, v.x), fmax(u.y, v.y));
}

static inline float2 clamp(const float2 &u, float a, float b) {
    return float2(clamp(u.x, a, b), clamp(u.y, a, b)); }

static inline float line2dist(float a, float b, float c, float2 p) {
    return a * p.x + b * p.y + c;
}

static inline float2 lhNormal(const float2 &u) {
    return normalize(float2(-u.y, u.x));
}

static inline float2 rhNormal(const float2 &u) {
    return normalize(float2(u.y, -u.x));
}

