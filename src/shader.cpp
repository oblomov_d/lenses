#include <src/shader.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <GL/glew.h>
#include <GL/gl.h>

#define SHREAD_ERR  1
#define SHREAD_SUCC 0

bool readFileCstring(const char* fname, char* &res){
    FILE *fp = fopen(fname, "r");
    res = nullptr;
    if (fp != NULL){
	fseek(fp, 0, SEEK_END);
	long fsize = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	res = new char[fsize + 1];
	fread(res, 1, fsize, fp);
	res[fsize] = '\0';
	fclose(fp);

	return true;
    }
    return false;
}

void checkShader(GLuint ShaderID){
    GLint result = GL_FALSE;
    int loglen;
    
    glGetShaderiv(ShaderID, GL_COMPILE_STATUS, &result);
    if (result != GL_TRUE){
	glGetShaderiv(ShaderID, GL_INFO_LOG_LENGTH, & loglen);
	if ( loglen > 0) {
	    printf("error with shader, message len is %d \n",  loglen);
	    char *errmsg = new char[ loglen + 1];
	    glGetShaderInfoLog(ShaderID,  loglen, NULL, errmsg);
	    printf("%s\n", errmsg);
	    delete [] errmsg;
	}
    }
}

void checkProgram(GLuint ProgramID){
    GLint result = GL_FALSE;
    int loglen;
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &result);
    if (result != GL_TRUE){
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &loglen);
	printf("error while linking program:\n");
	char *errmsg = new char[loglen + 1];
	glGetProgramInfoLog(ProgramID, loglen, NULL, errmsg);
	printf("%s\n", errmsg);
	delete [] errmsg;
    }
}


void compileShaderFile(GLuint ShaderID, char* shaderSrc,
		       const char* file_path){
    printf("Compiling shader %s\n", file_path);
    glShaderSource(ShaderID, 1, &shaderSrc, NULL);
    glCompileShader(ShaderID);

    checkShader(ShaderID);
}

GLuint loadShaders(const char* vert_file_path, const char* frag_file_path){
    GLuint ProgramID;
    GLuint VertShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    char *vertSrc, *fragSrc;
    int vertReadRes, fragReadRes;
    vertReadRes = readFileCstring(vert_file_path, vertSrc);
    fragReadRes = readFileCstring(frag_file_path, fragSrc);

    if (!vertReadRes){
	fprintf(stderr, "missing vertex shader file %s!\n", vert_file_path);
    }
    if (!fragReadRes){
	fprintf(stderr, "missing fragment shader file %s!\n", frag_file_path);
    }
    if (!fragReadRes || !vertReadRes){
	return 0;
    }

    compileShaderFile(VertShaderID, vertSrc, vert_file_path);
    compileShaderFile(FragShaderID, fragSrc, frag_file_path);

    printf("Linking shader programs\n");

    ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertShaderID);
    glAttachShader(ProgramID, FragShaderID);
    glLinkProgram(ProgramID);

    GLint program_linked;
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &program_linked);
    if (program_linked != GL_TRUE){
	GLsizei log_length = 0;
	
    }

    glDetachShader(ProgramID, VertShaderID);
    glDetachShader(ProgramID, FragShaderID);

    glDeleteShader(VertShaderID);
    glDeleteShader(FragShaderID);

    delete [] vertSrc;
    delete [] fragSrc;

    return ProgramID;
}
