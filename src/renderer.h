#pragma once

#include <GL/glew.h>
#include <GL/gl.h>
#include <glfw3.h>

#include <AntTweakBar.h>

#include <iostream>

#include <src/mesh.h>
#include <src/scene.h>

#include <src/input.h>


struct Renderer {
    bool running;
    int width, height;
    GLFWwindow *window;

    Scene scene;
    Input inp;

    GLuint programID;
    GLuint uni_widthID, uni_heightID, uni_scalingID;
    GLuint uni_yshiftID;
    TwBar *bar;

    Renderer();
    ~Renderer();

    void initGL();
    void initTweak();
    void run();
    void input();
    void render();
};


