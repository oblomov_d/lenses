#include <src/shapes.h>

#include <src/mesh.h>

#include <cstdlib>
#include <cmath>

#include <utility>
#include <iostream>

// #define DEBUGGING
#undef DEBUGGING

EmptyShape DUMMY_SHAPE;

Mesh debug_mesh;

const float3 bg_color(0.4f, 0.4f, 0.6f);

std::ostream & operator << (std::ostream &os, const seg2 &s) {
    os << "seg2(" << s.a << ", " << s.b << ")";
    return os;
}

ray2::ray2(float sx, float sy, float dx, float dy) :
    pos(float2(sx, sy)), dir(normalize(float2(dx, dy))) {
    // std::cout << *this << " xd xd \n";
}

std::ostream & operator << (std::ostream &os, const ray2 &r) {
    os << r.pos << " + t*" << r.dir;
    return os;
}

std::ostream & operator << (std::ostream &os, const RayPoint &rp) {
    os << "(" << rp.t << "; " << rp.p << "; " << rp.n << ")";
    return os;
}

std::ostream & operator << (std::ostream &os, const IntersectRes &ir) {
    os << "{ intersected : " << ir.intersect << "\n" <<
	  "  point       : " << ir.pt << "\n}\n";
    return os;
}

void seg2::putToMesh(Mesh &m, const float3 &color,
		     float thickness) const {
    float2 n = leftHandNormal(*this);
    n *= thickness;
    float2 p1, p2, p3, p4;
    p1 = a + n;
    p2 = b + n;
    p3 = b - n;
    p4 = a - n;

    m.reserve(2);
    m.addTriangle(triangle(p1, p2, p3), color);
    m.addTriangle(triangle(p3, p4, p1), color);
}

float2 leftHandNormal(const seg2 &s) {
    float2 tmp = normalize(s.b - s.a);
    return float2(-tmp.y, tmp.x);
}

float line2::dist(const float2 &p){
    return dot(dir, p) + offs;
}

void circle::printType() const {
    std::cout << "circle";
}

void box::printType() const {
    std::cout << "box";
}

void ShapeUnion::printType() const {
    std::cout << "union of (";
    a->printType();
    std::cout << ", ";
    b->printType();
    std::cout << ")";
}

void ShapeInt::printType() const {
    std::cout << "intersection of (";
    a->printType();
    std::cout << ", ";
    b->printType();
    std::cout << ")";
}

void ShapeSub::printType() const {
    std::cout << "subtraction of (";
    pos->printType();
    std::cout << " - ";
    neg->printType();
    std::cout << ")";
}

void putToMesh(const seg2 &s, Mesh &m, const float3 &color,
	       float thickness) {
    float2 n = leftHandNormal(s);
    n *= thickness;
    float2 p1, p2, p3, p4;
    p1 = s.a + n;
    p2 = s.b + n;
    p3 = s.b - n;
    p4 = s.a - n;

    m.reserve(2);
    m.addTriangle(triangle(p1, p2, p3), color);
    m.addTriangle(triangle(p3, p4, p1), color);
}

bool refract(float2 r, float2 n, float eta, float2 &res) {
    float r_dot_n = dot(r, n);
    float k = 1.0 - eta * eta * (1.0 - r_dot_n * r_dot_n); 
    // std::cout << "refracting, k = " << k << "\n"; 
    if (k < 0) {
	return false;
    }
    res = eta * r - (eta * r_dot_n + sqrtf(k)) * n;
    return true;
}

void reflect(float2 r, float2 n, float2 &res) {
    res = r - 2 * dot(n, r) * n;
}

ShapeInt::ShapeInt() {
    a = nullptr;
    b = nullptr;
}

ShapeInt* makeBiconvex(float2 p, float w, float h, float r) {
    ShapeInt *res = new ShapeInt();
    ShapeInt *subres = new ShapeInt();
    float shift = r - w * 0.5;
    float2 p1 = p;
    p1.x -= shift; 
    float2 p2 = p;
    p2.x += shift;
    subres->a = new circle(p1, r);
    subres->b = new circle(p2, r); 

    res->a = new box(p, w, h);
    res->b = subres;

    return res;
}

ShapeInt* makePlanoConvex(float2 p, float w, float h, float r) {
    ShapeInt *res = new ShapeInt();
    float shift = r - w * 0.5;
    float2 circle_p = p;
    circle_p.x += shift;
    res->a = new box(p, w, h);
    res->b = new circle(circle_p, r);

    return res;
}

ShapeInt::~ShapeInt() {
    if (a != nullptr) 
	delete a;
    if (b != nullptr)
	delete b;
}

void ShapeInt::move(float2 ofs) {
    a->move(ofs);
    b->move(ofs);
}

bool ShapeInt::inside(float2 p) const {
    return a->inside(p) && b->inside(p);
}

IntersectRes ShapeInt::intersect(const ray2 &r) const {
    IntersectRes res;
    IntersectRes int_a = a->intersect(r);
    IntersectRes int_b = b->intersect(r);
    float2 tmp_rpos = r.pos + r.dir * LENSE_EPS;
    bool a_inside = a->inside(tmp_rpos);
    bool b_inside = b->inside(tmp_rpos);
    // a->printType();
    // std::cout << "_x_";
    // b->printType();
    // std::cout << ":\n";
    // std::cout << a_inside << "_x_" << b_inside << "\n";
    // std::cout << int_a << ";\n" << int_b << ".\n";
    res.intersect = false;
    if (a_inside) {
	if (b_inside) {
	    if (int_a.pt.t > LENSE_EPS) {
		res = int_a;
		if (int_b.pt.t > LENSE_EPS && int_b.pt.t < int_a.pt.t) {
		    res = int_b;
		}
	    } else if (int_b.pt.t > LENSE_EPS) {
		res = int_b;
	    }
	} else {
	    if (int_b.pt.t > LENSE_EPS) {
		res = int_b;
		tmp_rpos = res.pt.p + r.dir * LENSE_EPS;
		// std::cout << "tmp_rpos = " << tmp_rpos << "\n";
		// std::cout << a->inside(tmp_rpos) << " " <<
		//     b->inside(tmp_rpos) << "\n";
		res.intersect = this->inside(tmp_rpos);
	    } else {
		res.intersect = false;
	    }
	}
    } else {
	if (b_inside) {
	    if (int_a.pt.t > LENSE_EPS) {
		res = int_a;
		tmp_rpos = res.pt.p + r.dir * LENSE_EPS;
		res.intersect = this->inside(tmp_rpos);
	    } else {
		res.intersect = false;
	    }
	} else {
	    res = int_a.intersect ? int_a : int_b;
	    res = (res.pt.t > int_b.pt.t || res.pt.t < 0) ? res : int_b;
	    if (res.intersect) {
		tmp_rpos = res.pt.p + r.dir * LENSE_EPS;
		res.intersect = this->inside(tmp_rpos);
	    }
	}
    }
    return res;
}

float2 meshIntersect(float2 *ap, float2 *bp, int cnt_a, int cnt_b,
		     int a_start_ind, int &b_next_ind) {
    float2 res;
    float2 ap1 = ap[a_start_ind-1];
    float2 ap2 = ap[a_start_ind];
    seg2 s1(ap1, ap2);

    float2 bp1;
    float2 bp2;
    for (int i = 0; i < cnt_b; i++) {
	int i2 = (i + 1) % cnt_b;
	bp1 = bp[i];
	bp2 = bp[i2];
	seg2 s2(bp1, bp2);
	if (intersect(s1, s2, res)) {
	    b_next_ind = i;
	    return res;
	} 
    } 
    return ap2;
}

void printBoolArray(bool *a, int cnt) {
    for (int i = 0; i < cnt; i++) {
	std::cout << (int) a[i];
    }
    std::cout << "\n";
}

// ASSUMES SAME CLOCK-WISE POINT ORDER IN MESHES
// Could be modified to handle disjoint meshes
void ShapeInt::putToMesh(Mesh &m, const float3 &color) const {
    Mesh ma, mb;
    // not initializing meshes because we don't need opengl buffers
    a->putToMesh(ma, bg_color);
    b->putToMesh(mb, bg_color);
    
    triangle *ta = &ma.vertex_v[0];
    triangle *tb = &mb.vertex_v[0];
    int a_cnt = ma.elem_cnt / 3;
    int b_cnt = mb.elem_cnt / 3;

    intersectTriangleArrays(ta, tb, a_cnt, b_cnt, m, color);
}

ShapeSub::ShapeSub() {
    pos = nullptr;
    neg = nullptr;
}

//todo: slight offset, don't forget to adjust aabb in lense too
ShapeSub* makePlanoConcave(float2 p, float d, float w, float h, float r) {
    ShapeSub *res = new ShapeSub;

    float circle_xofs = r - (0.5*w - d);
    float2 circle_pos = p;
    circle_pos.x += circle_xofs;
    
    res->pos = new box(p, w, h);
    res->neg = new circle(circle_pos, r);
    
    return res;
}

ShapeSub* makeBiconcave(float2 p, float d, float w, float h, float r) {
    ShapeSub *res = new ShapeSub;
    ShapeSub *subRes = new ShapeSub;

    float circle_xofs = r + 0.5 * (w - d); 
    float2 circle1_p = p;
    circle1_p.x -= circle_xofs;
    float2 circle2_p = p;
    circle2_p.x += circle_xofs;
    
    subRes->pos = new box(p, w, h);
    subRes->neg = new circle(circle1_p, r);

    res->pos = subRes;
    res->neg = new circle(circle2_p, r);
    
    return res;
}

ShapeSub* makeMeniscus(float2 p, float d, float w, float h,
		       float r1, float r2) {
    ShapeSub *res = new ShapeSub;
    ShapeInt *subres = new ShapeInt;

    float2 p1 = p;
    p1.x += -d * 0.5 + r1;
    float2 p2 = p;
    p2.x += +d * 0.5 + r2;
    
    subres->a = new circle(p1, r1);
    subres->b = new box(p, w, h);
    res->pos = subres;
    res->neg = new circle(p2, r2);
    
    return res;
}

ShapeSub::~ShapeSub() {
    if (pos != nullptr)
	delete pos;
    if (neg != nullptr)
	delete neg;
}

bool ShapeSub::inside(float2 p) const {
    return pos->inside(p) && !neg->inside(p);
} 

void ShapeSub::move(float2 ofs) {
    pos->move(ofs);
    neg->move(ofs);
}

IntersectRes ShapeSub::intersect(const ray2 &r) const {
    IntersectRes res;
    res.intersect = false;

    float2 tmp_rpos = r.pos + LENSE_EPS * r.dir;
    bool pos_inside = pos->inside(tmp_rpos);
    bool neg_inside = neg->inside(tmp_rpos);
    
    IntersectRes pos_res = pos->intersect(r);
    IntersectRes neg_res1 = neg->intersect(r);
    IntersectRes neg_res2;
    ray2 tmp_ray;
    float tmp_t;
    tmp_ray.pos = neg_res1.pt.p;
    tmp_ray.dir = r.dir;
    neg_res2 = neg->intersect(tmp_ray);

    if (pos_res.pt.t > LENSE_EPS) {
	if (neg_inside) {
	    float2 tmp_neg1_pt = neg_res1.pt.p + LENSE_EPS * r.dir;
	    if (neg_res1.intersect && pos->inside(tmp_neg1_pt)){
		res = neg_res1;
		res.pt.n = -res.pt.n;
	    } else {
		res.intersect = false;
	    }
	} else {
	    if (pos_inside) {
		if (neg_res1.pt.t > LENSE_EPS) {
		    res = neg_res1;
		    res.pt.n = -res.pt.n;
		    if (pos_res.pt.t < res.pt.t) {
			res = pos_res;
		    }
		} else {
		    res = pos_res;
		}
	    } else {
		if (neg_res1.pt.t > LENSE_EPS) {
		    if (pos_res.pt.t < neg_res1.pt.t) {
			res = pos_res;
		    } else {
			// danger zone
			float2 tmp_neg2_pt = neg_res2.pt.p +
			    LENSE_EPS * r.dir;
			if (pos->inside(tmp_neg2_pt)) {
			    tmp_t = neg_res1.pt.t;
			    res = neg_res2;
			    res.pt.n = -res.pt.n;
			    res.pt.t += tmp_t;
			} else {
			    res = pos_res;
			}
		    }
		}
	    }
	}
    } else {
	res.intersect = false;
    }
    
    return res;
}

void ShapeSub::putToMesh(Mesh &m, const float3 &color) const {
    Mesh ma, mb;
    // not initializing meshes because we don't need opengl buffers
    pos->putToMesh(ma, bg_color);
    neg->putToMesh(mb, bg_color);

    pos->putToMesh(m, color);
    // neg->putToMesh(m, bg_color);

    triangle *ta = &ma.vertex_v[0];
    triangle *tb = &mb.vertex_v[0];
    int a_cnt = ma.elem_cnt / 3;
    int b_cnt = mb.elem_cnt / 3;

    intersectTriangleArrays(ta, tb, a_cnt, b_cnt, m, bg_color);

    //subtractTriangleArrays(ta, tb, a_cnt, b_cnt, m, color);
}

void circle::move(float2 ofs) {
    pos += ofs;
}

bool circle::inside(float2 p) const {
    return dotsqr(p - pos) < r * r;
}

IntersectRes circle::intersect(const ray2 &r) const {
    IntersectRes res;
    float2 p1, p2;
    float2 n1, n2;
    float t1, t2;
    res.intersect = ::intersect(r, *this, p1, p2, t1, t2);
    if (res.intersect) {
	res.intersect = false;
	if (t1 > LENSE_EPS) {
	    res.intersect = true;
	    n1 = normalize(p1 - pos);
	    res.pt = RayPoint(t1, p1, n1);
	}
	if (t2 > LENSE_EPS && (t2 < t1 || !res.intersect)) {
	    res.intersect = true;
	    n2 = normalize(p2 - pos);
	    res.pt = RayPoint(t2, p2, n2);
	}
    }
    return res;
}

void circle::putToMesh(Mesh &m, const float3 &color) const {
    const int detail = 90;
    float angle = 0.0f;
    float angle_increment = 2 * M_PI / float(detail);
    float2 prev_p, p;

    m.reserve(detail);

    float tmp_angle = angle + angle_increment;
    p = pos + r * float2(sin(tmp_angle), cos(tmp_angle));
    for (int i = 0; i < detail; i++) {
	prev_p = p;
	p = pos + r * float2(sin(angle), cos(angle)); 
	m.addTriangle(triangle(pos, prev_p, p), color);

	angle -= angle_increment;
    }
    #ifdef DEBUGGING
    std::cout << "put circle to mesh, vertex_v size is " <<
	m.vertex_v.size() << "\n";
    #endif
}

void box::move(float2 ofs) {
    for (int i = 0; i < 4; i++) {
	v[i] += ofs;
    }
    initEdges();
}

bool box::inside(float2 p) const {
    float2 normal;
    float tmp;
    for (int i = 0; i < 4; i++) {
	normal = leftHandNormal(edges[i]);
	tmp = dot(normal, p - v[i]);
	if (tmp > 0) {
	    return false;
	}	
    }
    return true;
}

IntersectRes box::intersect(const ray2 &r) const {
    IntersectRes res;
    float tmp_t;
    float2 tmp_p, tmp_n;
    bool tmp_intres;

    for (int i = 0; i < 4; i++) {
	tmp_intres = ::intersect(r, edges[i], tmp_p, tmp_t);
	if (tmp_intres && tmp_t > LENSE_EPS) {
	    if (res.intersect) {
		if (tmp_t < res.pt.t) {
		    tmp_n = leftHandNormal(edges[i]);
		    res.pt = RayPoint(tmp_t, tmp_p, tmp_n);
		}
		break;
	    } else {
		res.intersect = true;
		tmp_n = leftHandNormal(edges[i]);
		res.pt = RayPoint(tmp_t, tmp_p, tmp_n);
	    }
	}
    }
    return res;
}

void box::putToMesh(Mesh &m, const float3 &color) const {
    m.reserve(2);
    m.addTriangle(triangle(v[0], v[1], v[2]), color);
    m.addTriangle(triangle(v[2], v[3], v[0]), color);
    #ifdef DEBUGGING
    std::cout << "put box to mesh, vertex_v size is " <<
	m.vertex_v.size() << "\n";
    #endif 
}

ray2 randomRay2(){
    float sx = float(rand()) / float(RAND_MAX);
    float sy = float(rand()) / float(RAND_MAX);
    float dx = float(rand()) / float(RAND_MAX);
    float dy = float(rand()) / float(RAND_MAX);
    return ray2(sx, sy, dx, dy);
}

bool intersect(const ray2 &r1, const ray2 &r2, float2 &res, float &t){
    line2 l2(r2);
    float r1_dir_dot = dot(l2.dir, r1.dir);

    if (fabs(r1_dir_dot) < LENSE_EPS) {
	// if (fabs(skew(r1.dir, r2.pos - r1.pos)) > LENSE_EPS) {
	    return false; 
	// }
    }
    
    t = - l2.dist(r1.pos) / r1_dir_dot;
    
    res = r1.pos + t * r1.dir;
    float r2_int_dir = dot(res - r2.pos, r2.dir);
    if (r2_int_dir < 0 || t < 0) {
	return false;
    }
    return true; 
}

bool intersect(const ray2 &r1, const ray2 &r2, float2 &res){
    float t;
    bool intres = intersect(r1, r2, res, t);
    if (t < 0) {
	return false;
    }
    return intres;
}

bool intersect(const ray2 &r1, const seg2 &s2, float2 &res, float &t){
    ray2 r2(s2);
    #ifdef DEBUGGING
    std::cout << "intersecting ray: " << r1 << " with " << s2 << "\n";
    #endif

    bool int_check = intersect(r1, r2, res, t);
    if (!int_check){
	// std::cout << "failed int_check\n";
	return false;
    }
    float s2_int_dir2 = dot(res - s2.b, s2.a - s2.b);
    if (s2_int_dir2 < 0) {
	// std::cout << "failed s2_int_dir2\n";
	return false;
    }
    #ifdef DEBUGGING
    std::cout << "intersection successful\n";
    #endif
    return true;
}

bool intersect(const ray2 &r1, const seg2 &s2, float2 &res){
    float t;
    bool intres = intersect(r1, s2, res, t);
    if (t < 0) {
	return false;
    }
    return intres;
}

bool intersect(const seg2 &s1, const ray2 &r2, float2 &res, float &t){
    ray2 r1(s1);

    bool int_check = intersect(r1, r2, res, t);
    if (!int_check){
	return false;
    }
    float s1_int_dir2 = dot(res - s1.b, s1.a - s1.b);
    if (s1_int_dir2 < 0) {
	return false;
    }
    return true;
}

bool intersect(const seg2 &s1, const ray2 &r2, float2 &res){
    float t;
    // q
    bool intres = intersect(s1, r2, res, t);
    if (t < 0) {
	return false;
    }
    return intres;
}

bool intersect(const seg2 &s1, const seg2 &s2, float2 &res, float &t){
    ray2 r1(s1);
    ray2 r2(s2);

    bool int_check = intersect(r1, r2, res, t);
    if (!int_check){
	return false;
    }
    float s1_int_dir2 = dot(res - s1.b, s1.a - s1.b);
    if (s1_int_dir2 < 0) {
	return false;
    }
    float s2_int_dir2 = dot(res - s2.b, s2.a - s2.b);
    if (s2_int_dir2 < 0) {
	return false;
    }
    return true;
}

bool intersect(const seg2 &s1, const seg2 &s2, float2 &res){
    float t;
    return intersect(s1, s2, res, t);
}

bool intersect(const float2 &s1a, const float2 &s1b,
	       const float2 &s2a, const float2 &s2b, float2 &res) {
    float t;
    seg2 s1(s1a, s1b);
    seg2 s2(s2a, s2b);
    return intersect(s1, s2, res, t);
}

bool intersect(const float2 &s1a, const float2 &s1b,
	       const float2 &s2a, const float2 &s2b, float2 &res,
	       float &t) {
    seg2 s1(s1a, s1b);
    seg2 s2(s2a, s2b);
    return intersect(s1, s2, res, t);
}

bool intersect(const ray2 &r1, const circle &c2, float2 &p1, float2 & p2,
	       float &t1, float &t2){
    float a = dot(r1.dir, r1.dir);
    float b = dot(r1.pos, r1.dir) - dot(c2.pos, r1.dir);
    float c = dotsqr(r1.pos - c2.pos) - SQR(c2.r);
    float d = sqrt(b*b - a*c);
    if (d < 0 || fabs(a) < LENSE_EPS) {
	return false;
    }
    t1 = -(b + d) / a;
    t2 = -(b - d) / a;
    p1 = r1.pos + t1 * r1.dir;
    p2 = r1.pos + t2 * r1.dir;
    return true;
}

bool intersect(const ray2 &r1, const circle &c2, float2 &p1, float2 & p2){
    float t1, t2;
    return intersect(r1, c2, p1,  p2, t1, t2);
}


float triangleSDF(const triangle &t, const float2 &p) {
    float2 e0 = t.b - t.a;
    float2 e1 = t.c - t.b;
    float2 e2 = t.a - t.c;
    float2 v0 = p - t.a;
    float2 v1 = p - t.b;
    float2 v2 = p - t.c;
    float2 pq0 = v0 - e0 * clamp(dot(v0, e0) / dot(e0, e0), 0.0, 1.0);
    float2 pq1 = v1 - e1 * clamp(dot(v1, e1) / dot(e1, e1), 0.0, 1.0);
    float2 pq2 = v2 - e2 * clamp(dot(v2, e2) / dot(e2, e2), 0.0, 1.0);
    float s = copysign(1.0f, skew(e0, e2));
    float2 d0 = float2(dot(pq0, pq0), s * skew(v0, e0));
    float2 d1 = float2(dot(pq1, pq1), s * skew(v1, e1));
    float2 d2 = float2(dot(pq2, pq2), s * skew(v2, e2));
    float2 d = min(min(d0, d1), d2);
    s = copysign(1.0f, d.y);
    return -sqrtf(d.x) * s;
}

// assumes clockwise point order in triangle
// not anymore
bool inside(const triangle &t, const float2 &p) {
    float tmp;
    bool sgn1, sgn2, sgn3;
    tmp = skew(t.b - t.a, p - t.a);
    sgn1 = std::signbit(tmp);
    tmp = skew(t.c - t.b, p - t.b);
    sgn2 = std::signbit(tmp);
    tmp = skew(t.a - t.c, p - t.c);
    sgn3 = std::signbit(tmp);

    return sgn1 == sgn2 && sgn1 == sgn3;
}

inline float2 supportPoint(float2 *av, float2 *bv, float2 &dir) {
    float tmp;
    float max_a = -1e20;
    float max_b = -1e20;
    int max_ap, max_bp;
    for (int i = 0; i < 3; i++) {
	tmp = dot(av[i], dir);
	if (tmp > max_a) {
	    max_a = tmp;
	    max_ap = i;
	}
	tmp = dot(bv[i], -dir);
	if (tmp > max_b) {
	    max_b = tmp;
	    max_bp = i;
	}
    }
    return av[max_ap] - bv[max_bp];
}

float triangleArea(float2 a, float2 p, float2 q) {
    return p.x * q.y - p.y * q.x + a.x * (p.y - q.y) + a.y * (q.x - p.x);
}

float triangelIntersectArea(triangle &a, triangle &b) {
    return 0;
}

inline void gjkLineCheck(float2 &ab, float2 &ao, float2 &dir,
			 int &simp_cnt) {
    if (dot(ab, ao) > 0) {
	float abo_skew = skew(ab, ao);
	dir = cross2d(ab, -abo_skew);
    } else {
	simp_cnt = 1;
	dir = ao;
    }
}

bool triangleIntersectCheck(triangle &a, triangle &b) {
    float2 *av = &a.a;
    float2 *bv = &b.a;
    float2 tmp_p;
    float tmp_t;

    // float2 simplex[3];

    // float2 &ap = simplex[0];
    // float2 &bp = simplex[1];
    // float2 &cp = simplex[2];
    
    // int simp_cnt = 0;

    // float2 dir(1.0f, 0.0f);
    // float2 tmp_sup;
    
    // tmp_sup = supportPoint(av, bv, dir);
    // simplex[0] = tmp_sup;
    // simp_cnt++;
    // dir = -dir;
    
    // while (true) {
    // 	tmp_sup = supportPoint(av, bv, dir);
    // 	if (dot(tmp_sup, dir) < 0) {
    // 	    return false;
    // 	}
    // 	simplex[2] = simplex[1];
    // 	simplex[1] = simplex[0];
    // 	simplex[0] = tmp_sup;
    // 	simp_cnt = simp_cnt == 3 ? 3 : simp_cnt + 1;

    // 	float2 ab = bp - ap;
    // 	float2 ao =    - ap;
    // 	float2 ac;
    // 	float abc_skew;
    // 	float aco_skew;
    // 	float2 abcc;
    // 	float2 ab_abc;
    // 	switch (simp_cnt) {
    // 	case 2:
    // 	    gjkLineCheck(ab, ao, dir, simp_cnt);
    // 	    break;
    // 	case 3:
    // 	    ac = cp - ap;
    // 	    abc_skew = skew(ab, ac);
    // 	    abcc = cross2d(ac, abc_skew);

    // 	    if (dot(abcc, ao) > 0) {
    // 		if (dot(ac, ao) > 0) {
    // 		    simplex[0] = simplex[0];
    // 		    simplex[1] = simplex[2];
    // 		    simp_cnt = 2;
    // 		    aco_skew = skew(ac, ao);
    // 		    dir = cross2d(ac, aco_skew); 
    // 		} else {
    // 		    simp_cnt = 2;
    // 		    gjkLineCheck(ab, ao, dir, simp_cnt); 
    // 		}
    // 	    } else {
    // 		ab_abc = cross2d(ab, -abc_skew);
    // 		if (dot(ab_abc, ao) > 0) {
    // 		    simp_cnt = 2;
    // 		    gjkLineCheck(ab, ao, dir, simp_cnt);
    // 		} else {
    // 		    return true;
    // 		}
    // 	    }
    // 	    break;
    // 	default:
    // 	    std::cout << "fatal error in gjk test\n";
    // 	    break;
    // 	}
    // }
    // return false;
    
    for (int i = 0; i < 3; i++) {
    	int i2 = (i+1) % 3;
    	if (inside(a, bv[i])){
	    std::cout << "not adding triangle because point is inside\n";
    	    return true;
	}
    	for (int j = 0; j < 3; j++) {
    	    int j2 = (j+1) % 3;
    	    if (intersect(av[i], av[i2], bv[j], bv[j2], tmp_p, tmp_t)) {
		float tmp_len;
		tmp_len = dotsqr(av[i] - tmp_p);
		if (tmp_len < LENSE_EPS)
		    continue;
		tmp_len = dotsqr(av[i2] - tmp_p);
		if (tmp_len < LENSE_EPS)
		    continue;
		tmp_len = dotsqr(bv[j] - tmp_p);
		if (tmp_len < LENSE_EPS)
		    continue;
		tmp_len = dotsqr(bv[j2] - tmp_p);
		if (tmp_len < LENSE_EPS)
		    continue;
		std::cout << "insterescted subtriangles " <<
		    a << ", " << b << " at " << tmp_p  << ", " <<
		    tmp_t << "\n";
    		return true;
    	    }
    	}
    }
    return false;
}

void intersectTriangleArrays(triangle *a, triangle *b,
			     int a_cnt, int b_cnt,
			     Mesh &m, const float3 &new_color) {
    for (int i = 0; i < a_cnt; i++) {
	for (int j = 0; j < b_cnt; j++) {
	    intersectTriangles(a[i], b[j], m, new_color);
	} 
    }
}

void intersectTriangles(triangle &a, triangle &b, Mesh &m,
			const float3 &new_color) {
    float2 *av = &a.a;
    float2 *bv = &b.a;
    float2 tmp;
    // error potential if one triangle point
    // is exactly on the side of the other: 
    float2 res_pts[6];
    int res_cnt = 0;

    for (int i = 0; i < 3; i++) {
	if (inside(b, av[i])) { 
	    res_pts[res_cnt++] = av[i];
	}
	if (inside(a, bv[i])) {
	    res_pts[res_cnt++] = bv[i];
	}
    }
    
    for (int i = 0; i < 3; i++) {
	int i2 = (i+1) % 3;
	for (int j = 0; j < 3; j++) {
	    int j2 = (j+1) % 3;
	    if (intersect(av[i], av[i2], bv[j], bv[j2], tmp)) { 
		bool should_add = true;
		for (int k = 0; k < res_cnt && should_add; k++) {
		    should_add = length(tmp - res_pts[k]) > LENSE_EPS;
		}
		if (should_add) {
		    res_pts[res_cnt++] = tmp;
		}
	    }
	    #ifdef DEBUGGING
	    if (res_cnt > 6) {
		std::cout << "too many points in triangle intersection\n"; 
	    }
	    #endif
	}
    }
    if (res_cnt < 3) {
	return;
    }
    
    for (int i = 0; i < res_cnt - 1; i++) {
	float min_x = res_pts[i].x;
	int min_ind = i;
	for (int j = i + 1; j < res_cnt; j++) {
	    if (res_pts[j].x < min_x) {
		min_ind = j;
		min_x = res_pts[j].x;
	    }
	}
	tmp = res_pts[min_ind];
	res_pts[min_ind] = res_pts[i];
	res_pts[i] = tmp;
    }

    float2 p1, p2, p3, p4;
    bool prev_orient, orient;
    float tmp_skew;

    p1 = res_pts[0];
    p2 = res_pts[1];
    p3 = res_pts[2];
    
    tmp_skew = skew(p2 - p1, p3 - p1);
    orient = ! std::signbit(tmp_skew);

    if (orient) {
	m.addTriangle(triangle(p1, p3, p2), new_color);
    } else {
	m.addTriangle(triangle(p1, p2, p3), new_color);
    }
    
    for (int i = 1; i < res_cnt - 2; i++) {
	prev_orient = orient;
	p4 = res_pts[i+2];

	tmp_skew = skew(p3 - p1, p4 - p1);
	orient = ! std::signbit(tmp_skew);
	if (prev_orient == orient) {
	    p2 = p3;
	    p3 = p4;
	} else {
	    p1 = p2;
	    p2 = p3;
	    p3 = p4;
	}

	tmp_skew = skew(p2 - p1, p3 - p1);
	orient = ! std::signbit(tmp_skew);
	
	if (orient) {
	    m.addTriangle(triangle(p1, p3, p2), new_color);
	} else {
	    m.addTriangle(triangle(p1, p2, p3), new_color);
	}
    }
}

void subtractTriangleArrays(triangle *a, triangle *b,
			     int a_cnt, int b_cnt,
			     Mesh &m, const float3 &new_color) {
    for (int i = 0; i < a_cnt; i++) {
	for (int j = 0; j < b_cnt; j++) {
	    subtractTriangles(a[i], b[j], m, new_color);
	} 
    }
}

// a - b
void subtractTriangles(triangle &a, triangle &b, Mesh &m,
		       const float3 &new_color) {
    float2 *av = &a.a;
    float2 *bv = &b.a;
    float2 tmp;
    // error potential if one triangle point
    // is exactly on the side of the other: 
    float2 res_pts[9];
    int res_cnt = 0;
    int a_inside[3];

    
    for (int i = 0; i < 3; i++) {
	if (!inside(b, av[i])) {
	    a_inside[i] = res_cnt;
	    res_pts[res_cnt++] = av[i];
	    std::cout << "adding " << av[i] << " to points, " <<
		res_cnt << " \n";
	} else {
	    a_inside[i] = -1;
	}
	if (inside(a, bv[i])) {
	    res_pts[res_cnt++] = bv[i];
	    std::cout << "adding " << bv[i] << " to points, " <<
		res_cnt << " \n";
	}
    }
    
    for (int i = 0; i < 3; i++) {
	int i2 = (i+1) % 3;
	for (int j = 0; j < 3; j++) {
	    int j2 = (j+1) % 3;
	    if (intersect(av[i], av[i2], bv[j], bv[j2], tmp)) { 
		bool should_add = true;
		for (int k = 0; k < res_cnt && should_add; k++) {
		    should_add = length(tmp - res_pts[k]) > LENSE_EPS;
		}
		if (should_add) {
		    res_pts[res_cnt++] = tmp;
		    std::cout << "adding " << tmp << " to points, " <<
			res_cnt << " \n";
		}
	    }
	    #ifdef DEBUGGING
	    if (res_cnt > 9) {
		std::cout << "too many points in triangle intersection\n"; 
	    }
	    #endif
	}
    }
    int triangle_cnt = 0;
    int total_checked = 0;

    for (int i = 0; i < 3; i++) {
	if (a_inside[i] != -1) {
	    float2 p1 = av[i]; 
	    for (int j = 0; j < res_cnt - 1; j++) {
		if (j == a_inside[i])
		    continue;
		float2 p2 = res_pts[j];
		for (int k = j+1; k < res_cnt; k++) {
		    if (k == a_inside[i])
			continue;
		    float2 p3 = res_pts[k];
		    triangle tmp_tri(p1, p2, p3);
		    total_checked++;
		    if (!triangleIntersectCheck(b, tmp_tri)) {
			m.addTriangle(tmp_tri, new_color);
			std::cout << "added triangle: " << tmp_tri << "\n";
			triangle_cnt++;
		    } else {
			float3 bg_color(0.0f, 0.0f, 1.0f);
			m.addTriangle(tmp_tri, bg_color);
		    }
		} 
	    }
	}
    }
    std::cout << triangle_cnt << ", " << total_checked << "\n";
}

