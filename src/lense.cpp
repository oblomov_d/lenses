#include <src/lense.h>

#include <src/shapes.h>

#include <cstring>

#include <AntTweakBar.h>

extern EmptyShape DUMMY_SHAPE; 

float3 def_color(1.0f, 1.0f, 1.0f);
const float arrow_thickness = 0.1f;
const float arrow_default_len = 5.0f;
const float2 arrow_default_pos(-25, 0.0);

const int LENSE_NAME_CNT = 7;
const char* const LENSE_NAMES[] =
    {"empty", "pos_meniscus", "biconvex",
     "planoconvex", "planoconcave", "neg_meniscus", "biconcave"};

bool ltypeFromString(const char* cstr, LENSE_TYPE &res) {
    res = EMPTY;
    for (int i = 0; i < LENSE_NAME_CNT; i++) {
	if (0 == strcmp(cstr, LENSE_NAMES[i])) {
	    res = static_cast<LENSE_TYPE>(i);
	    return true;
	}
    }
    return false;
}

void TW_CALL lenseSetRefrC(const void *val, void *client_data) {
    LenseParams *lp = (LenseParams*) client_data;
    lp->refr_c = *(float*) val;
    lp->recalcF();
}

void TW_CALL lenseGetRefrC(void *val, void *client_data) {
    LenseParams *lp = (LenseParams*) client_data;
    *(float*) val = lp->refr_c;
}

void TW_CALL lenseSetF(const void *val, void *client_data) {
    LenseParams *lp = (LenseParams*) client_data;
    lp->focal_len = *(float*) val;
    std::cout << "changing value of focal len\n";
    lp->recalcRefr();
}

void TW_CALL lenseGetF(void *val, void *client_data) {
    LenseParams *lp = (LenseParams*) client_data;
    *(float*) val = lp->focal_len;
}

void TW_CALL lenseSetX(const void *val, void *client_data) {
    Lense *l = (Lense*) client_data;
    float tar_x = *(float*)val;
    float dx = tar_x - l->pos.x;
    l->move(float2(dx, 0));
}

void TW_CALL lenseGetX(void *val, void *client_data) {
    Lense *l = (Lense*) client_data;
    *(float*) val = l->pos.x;
}

void LenseParams::recalcRefr() {
    
}

void LenseParams::calcF(float n, float iR1, float iR2, float d) {
    refr_c = n;
    ir1 = iR1;
    ir2 = iR2;
    thick = d;
    recalcF();
}

void LenseParams::recalcF() {
    // float denom = (refr_c-1) *
    // 	(ir1 - ir2 + (refr_c-1)*thick * ir1 * ir2 / refr_c);
    // if (fabs(denom) < LENSE_EPS) {
    // 	can_focus = false;
    // 	focal_len = 0.0f;
    // } else {
    // 	can_focus = true;
    // 	focal_len = 1.0f / denom;
    // }
    float denom = (refr_c-1) * (ir1 - ir2);
    if (fabs(denom) < LENSE_EPS) {
	can_focus = false;
	focal_len = 0.0f;
    } else {
	can_focus = true;
	focal_len = 1.0f / denom;
    }
}

void Lense::initEmpty(float2 p) {
    pos = p;
    dims.x = -1e20;
    dims.y = -1e20;
    
    sh = &DUMMY_SHAPE;
    mesh.init();
    type = LENSE_TYPE::EMPTY;
    params.refr_c = 1.0f;
}

void Lense::initBiconvex(float2 p, float refr_c, float w, float h, float r) {
    pos = p;
    dims.x = w*0.5;
    dims.y = h*0.5;
    
    sh = makeBiconvex(p, w, h, r);
    // params.calcF(refr_c, 1.0f/r, -1.0f/r, w);
    params.focal_len = 6.0f;

    mesh.init();
    sh->putToMesh(mesh, def_color);
    type = LENSE_TYPE::BICONVEX;
}

void Lense::initPlanoConvex(float2 p, float refr_c, float w, float h,
			    float r) {
    pos = p;
    dims.x = w*0.5;
    dims.y = h*0.5;
    
    sh = makePlanoConvex(p, w, h, r);
    // params.calcF(refr_c, 1.0f/r, 0, w);
    params.focal_len = 8.0f;
    
    mesh.init();
    sh->putToMesh(mesh, def_color);
    type = LENSE_TYPE::PLANOCONVEX;
}

void Lense::initPlanoConcave(float2 p, float refr_c, float d,
			     float w, float h, float r){
    pos = p;
    dims.x = w*0.5;
    dims.y = h*0.5;
    
    sh = makePlanoConcave(p, d, w, h, r);
    // params.calcF(refr_c, 0, 1.0f/r, d);
    params.focal_len = -8.0f;
    
    mesh.init();
    sh->putToMesh(mesh, def_color);
    type = LENSE_TYPE::PLANOCONCAVE;
}

void Lense::initBiconcave(float2 p, float refr_c, float d, float w,
			  float h, float r) {
    pos = p;
    dims.x = w*0.5;
    dims.y = h*0.5;
    
    sh = makeBiconcave(p, d, w, h, r);
    params.calcF(refr_c, -1.0f/r, +1.0f/r, d);
    params.focal_len = -6.0f;

    mesh.init();
    sh->putToMesh(mesh, def_color);
    type = LENSE_TYPE::BICONCAVE;
}

void Lense::initMeniscus(float2 p, float refr_c, float d, float w,
			 float h, float r1, float r2) {
    pos = p;
    dims.x = w*0.5;
    dims.y = h*0.5;
    
    sh = makeMeniscus(p, d, w, h, r1, r2);
    // params.calcF(refr_c, 1.0/r1, 1.0/r2, d);
    mesh.init();
    sh->putToMesh(mesh, def_color);
    if (r1 < r2) {
	type = LENSE_TYPE::POS_MENISCUS;
	params.focal_len = 5.0f;
    } else {
	type = LENSE_TYPE::NEG_MENISCUS;
	params.focal_len = -5.0f;
    }
}

float2 Lense::getFocalPoint() const {
    float2 res = pos;
    res.x += params.focal_len;
    return res;
}

void Lense::move(const float2 &ofs) {
    pos += ofs;
    sh->move(ofs);
    mesh.move(ofs);
}

bool Lense::testIntersect(const Lense &other) const {
    bool dx, dy;
    dx = fabs(pos.x - other.pos.x) < (dims.x + other.dims.x); 
    dy = fabs(pos.y - other.pos.y) < (dims.y + other.dims.y); 
    return dx && dy;
}

bool Lense::testIntersect(const float2 &o_pos, const float2 &o_dims) const {
    bool dx, dy;
    dx = fabs(pos.x - o_pos.x) < (dims.x + o_dims.x); 
    dy = fabs(pos.y - o_pos.y) < (dims.y + o_dims.y); 
    return dx && dy;
}

bool Lense::inside(float2 p) const {
    return sh->inside(p);
}

bool Lense::insideAABB(float2 p) const {
    float2 dd = p - pos;
    return fabs(dd.x) < dims.x && fabs(dd.y) < dims.y;
}

void Lense::updateMesh() {
    mesh.clear();
    sh->putToMesh(mesh, def_color);
}

void Lense::cleanup() {
    #ifdef DEBUGGING
    std::cout << "deleting lense with ";
    sh->printType();
    std::cout << "\n";
    #endif
    if (sh != &DUMMY_SHAPE) {
	type = LENSE_TYPE::EMPTY;
	mesh.clear();
	delete sh;
	sh = &DUMMY_SHAPE;
    }
}

Arrow::Arrow() {
    v = seg2(arrow_default_pos, arrow_default_pos +
	       arrow_default_len * float2(0.0, 1.0));
    len = arrow_default_len;
    at_infinity = false;
    virtual_img = false;
}

Arrow::Arrow(float2 start, float len_in, bool virtual_in, float inf_sign) {
    v = seg2(start, start + len_in * float2(0.0, 1.0));
    len = len_in;
    at_infinity = false;
    virtual_img = virtual_in;
    infinity_sign = inf_sign;
}

Arrow::Arrow(float len_in, float inf_sign) {
    len = len_in;
    at_infinity = true;
    virtual_img = false;
    infinity_sign = inf_sign;
}

void Arrow::putToMesh(Mesh &m, const float3 &color) {
    if (at_infinity)
	return;
    float2 p1, p2, p3, p4;

    if (fabs(len) < LENSE_EPS) {
	p1 = v.a - arrow_thickness*2;
	p3 = v.a + arrow_thickness*2;
	p2 = float2(p3.x, p1.y);
	p4 = float2(p1.x, p3.y);
	m.reserve(2);
	m.addTriangle(triangle(p1, p2, p3), color);
	m.addTriangle(triangle(p3, p4, p1), color);
	return;
    }

    float2 n = leftHandNormal(v);
    n *= arrow_thickness;
    float2 tp1, tp2, tp3;
    float2 dir = normalize(v.b-v.a);
    float head_scale = (fabs(len) + LENSE_EPS) * 0.4;
    p1 = v.a + n;
    p2 = v.b + n - 2 * arrow_thickness * dir;
    p3 = v.b - n - 2 * arrow_thickness * dir;
    p4 = v.a - n;
    tp1 = v.b + 2 * arrow_thickness * normalize(v.b-v.a);
    tp2 = tp1 - normalize(tp1-p2) * head_scale;
    tp3 = tp1 - normalize(tp1-p3) * head_scale;

    m.reserve(3);
    m.addTriangle(triangle(p1, p2, p3), color);
    m.addTriangle(triangle(p3, p4, p1), color);
    m.addTriangle(triangle(tp1, tp2, tp3), color);
}

bool Arrow::inside(const float2 &pos) {
    float2 m = (v.a + v.b) * 0.5;
    float2 dims(arrow_thickness,
		arrow_thickness + fabs(v.b.y - v.a.y));
    float2 dif = pos - m;
    return fabs(dif.x) < dims.x && fabs(dif.y) < dims.y;
}

void Arrow::move(float dx) {
    v.b.x += dx;
    v.a.x += dx;
}

void Arrow::resize(float new_len) {
    float dlen = new_len - len;
    v.b.y += dlen;
    len = new_len;
}

void Arrow::castRays(std::vector<ray2> &rays, const Lense &l) {
    if (at_infinity) {
	rays.push_back(ray2(-1000, len, 1.0, 0.0));
	rays.push_back(ray2(-1000, 0, 1.0, 0.0));
    } else {
	ray2 r1 = ray2(v.b, float2(1.0, 0.0)); 
	rays.push_back(r1);
	ray2 r2 = ray2(v.b, l.pos - v.b);
	rays.push_back(r2);
	ray2 r3 = ray2(v.a, float2(1.0, 0.0));
	rays.push_back(r3);
    }
}

void TW_CALL arrowSetLen(const void *val, void *client_data) {
    Arrow *ap = (Arrow*) client_data;
    ap->resize(*(float*) val);
}

void TW_CALL arrowGetLen(void *val, void *client_data) {
    Arrow *ap = (Arrow*) client_data;
    *(float*) val = ap->len;
}

void TW_CALL arrowSetInfinite(const void *val, void *client_data) {
    Arrow *ap = (Arrow*) client_data;
    ap->at_infinity = *(bool*) val;
}

void TW_CALL arrowGetInfinite(void *val, void *client_data) {
    Arrow *ap = (Arrow*) client_data;
    *(bool*) val = ap->at_infinity;
}

void TW_CALL arrowSetX(const void *val, void *client_data) {
    Arrow *ap = (Arrow*) client_data;
    float tar_x = *(float*)val;
    float dx = tar_x - ap->v.a.x;
    ap->move(dx);
}

void TW_CALL arrowGetX(void *val, void *client_data) {
    Arrow *ap = (Arrow*) client_data;
    *(float*) val = ap->v.a.x;
}

