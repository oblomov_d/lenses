#include <src/vecmath.h>


std::ostream & operator << (std::ostream &os, const float2 &p) {
    os << "(" << p.x << ", " << p.y << ")";
    return os;
}

std::ostream & operator << (std::ostream &os, const triangle &t) {
    os << "{" << t.a << ", " << t.b << ", " << t.c << "}";
    return os;
}

